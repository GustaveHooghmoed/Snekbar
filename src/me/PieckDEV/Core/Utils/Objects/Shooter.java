package me.PieckDEV.Core.Utils.Objects;


import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.util.Vector;


public class Shooter {
    private double height;
    private Location location;


    public Shooter(Location location, double height) {

        this.location = location;

        this.height = (height - 0.3D);

    }


    public void execute() {

        for (double i = 0.0D; i > -(this.height * 15.0D); i -= 1.0D) {

            if (!this.location.clone().add(0.0D, i, 0.0D).getBlock().isLiquid()) {

                this.location.clone().add(0.0D, i, 0.0D).getBlock().setType(Material.AIR);

            }

            CustomFallingBlock block = new CustomFallingBlock(this.location.clone().add(0.0D, i, 0.0D), 19, (byte) 0);

            block.setVelocity(new Vector(0.0D, this.height, 0.0D));

            if (this.height + 0.3D >= 2.0D) {

                block.setRemoveTime(100L);

            }

            block.setAutomaticRemove(true);

        }

    }

}


