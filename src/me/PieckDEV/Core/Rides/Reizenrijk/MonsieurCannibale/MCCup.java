package me.PieckDEV.Core.Rides.Reizenrijk.MonsieurCannibale;


import me.CaptainXan.PardoesCraft.Main.Main;
import me.CaptainXan.PardoesCraft.Utils.LocationUtil;
import me.CaptainXan.PardoesCraft.Utils.Relatives.RelativeUtil;
import me.CaptainXan.PardoesCraft.Utils.Relatives.StandRelative;
import me.PieckDEV.Core.Utils.EntityUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Random;


public class MCCup {
    private static double distance;
    private ArmorStand center;
    private ArrayList<ArmorStand> stands = new ArrayList();
    private double angle = 0.0D;
    private double speed = 0.0D;
    private boolean changed = false;


    public MCCup(Location location, short color) {

        final ArmorStand center = location.getWorld().spawn(location, ArmorStand.class);

        center.setCustomName("MCCupCenter");

        center.setHelmet(new ItemStack(Material.DIAMOND_SWORD, 1, color));

        center.setVisible(false);

        center.setGravity(false);

        EntityUtil.setNoClip(center, true);


        this.center = center;


        boolean first = false;

        for (Location loc : LocationUtil.getCircleLocs(location, 1.0D, 7)) {

            if (first) {

                ArmorStand stand = loc.getWorld().spawn(loc.clone().add(0.0D, -0.6D, 0.0D), ArmorStand.class);

                stand.setCustomName("MCCup");

                stand.setGravity(false);

                stand.setVisible(false);

                EntityUtil.setNoClip(stand, true);

                this.stands.add(stand);

                RelativeUtil.relatives.put(stand, new StandRelative(center, stand));

            }

            first = true;

        }

        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.pl, new Runnable() {

            public void run() {

                int yaw = new Random().nextInt(360);

                LocationUtil.setM(center, yaw);

                MCCup.this.angle = center.getLocation().getYaw();

            }

        }, 20L);


        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.pl, new Runnable() {

            public void run() {

                for (ArmorStand stand : MCCup.this.stands) {

                    stand.setGravity(true);

                    StandRelative relative = RelativeUtil.relatives.get(stand);

                    relative.setDistance(MCCup.this.getDistance());

                    RelativeUtil.updateRelative(relative);

                    stand.setGravity(false);

                }

            }

        }, 30L);

    }


    protected double getDistance() {

        return distance;

    }


    public static void setDistance(double doub) {

        distance = doub;

    }


    public void setSpeed(double value) {

        this.speed = value;

        if (value > 0.0D) {

            if (!this.changed) {

                this.changed = true;

                for (ArmorStand stand : this.stands) {

                    stand.setGravity(true);

                    EntityUtil.setNoClip(stand, true);

                }


                new BukkitRunnable() {

                    public void run() {

                        if (MCCup.this.speed > 0.0D) {

                            LocationUtil.setM(MCCup.this.center, (float) (MCCup.this.angle += MCCup.this.speed));

                            for (ArmorStand stand : MCCup.this.stands) {

                                StandRelative relative = RelativeUtil.relatives.get(stand);

                                RelativeUtil.updateRelative(relative);

                                LocationUtil.setM(stand, LocationUtil.getAngle(stand.getLocation().toVector(), MCCup.this.center.getLocation().toVector()));

                            }

                        } else {

                            cancel();

                        }

                    }

                }.runTaskTimer(Main.pl, 0L, 0L);

            }

        } else {

            this.speed = 0.0D;

            this.changed = false;


            this.center.setGravity(false);

            EntityUtil.setNoClip(this.center, true);

            for (ArmorStand stand : this.stands) {

                stand.setGravity(false);

                EntityUtil.setNoClip(stand, true);

            }

        }

    }


    public void moveTo(Location location) {

        this.center.setGravity(true);

        EntityUtil.setNoClip(this.center, true);

        this.center.setVelocity(LocationUtil.getFromTo(this.center.getLocation(), location));

    }


    public void remove() {

        this.center.remove();

        for (ArmorStand stand : this.stands) {

            stand.remove();

        }

        for (Entity ent : EntityUtil.getEntitiesByName("MCCup")) {

            ent.remove();

        }

        for (Entity ent : EntityUtil.getEntitiesByName("MCCupCenter")) {

            ent.remove();

        }

    }


    public ArrayList<ArmorStand> getStands() {

        return this.stands;

    }

}


