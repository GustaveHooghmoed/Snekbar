package me.PieckDEV.Core.Managers;


import me.CaptainXan.PardoesCraft.Utils.Interfaces.Door;

import java.util.HashMap;


public class DoorManager {
    public static HashMap<String, Door> doors = new HashMap();


    public static void setup() {


        for (Door d : doors.values()) {

            d.spawn();

        }

    }


    public static void despawn() {

        for (Door d : doors.values()) {

            d.despawn();

        }

    }


    public static Door getDoorFromName(String name) {

        if (doors.containsKey(name.toLowerCase())) {

            return doors.get(name.toLowerCase());

        }

        return null;

    }

}


