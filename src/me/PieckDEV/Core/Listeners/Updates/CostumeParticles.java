package me.PieckDEV.Core.Listeners.Updates;


import me.CaptainXan.PardoesCraft.Events.UpdateEvent;
import me.CaptainXan.PardoesCraft.Managers.CostumeManager;
import me.CaptainXan.PardoesCraft.Utils.Enums.UpdateType;
import me.CaptainXan.PardoesCraft.Utils.ParticleUtil;
import net.minecraft.server.v1_12_R1.EnumParticle;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;


public class CostumeParticles
        implements Listener {

    @EventHandler
    public void onUpdate(UpdateEvent e) {

        if (e.getType() == UpdateType.TICK) {

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (CostumeManager.equippedCostume(p) != null) {

                    if (CostumeManager.equippedCostume(p).getName().equalsIgnoreCase("developer")) {

                        ParticleUtil.sendToLocation(p.getEyeLocation(), EnumParticle.ENCHANTMENT_TABLE, 0.5D, 0.2D, 0.5D, 1.0F, 5);

                    }

                    if ((CostumeManager.equippedCostume(p).getName().equalsIgnoreCase("dragon")) &&
                            (p.isOnGround())) {

                        ParticleUtil.sendToLocation(p.getLocation(), EnumParticle.DRAGON_BREATH, 0.5D, 0.0D, 0.5D, 0.0F, 5);

                    }

                    if ((CostumeManager.equippedCostume(p).getName().endsWith("ruiter")) &&
                            (p.isOnGround())) {

                        if (CostumeManager.equippedCostume(p).getName().equalsIgnoreCase("roderuiter")) {

                            ParticleUtil.sendToLocation(p.getLocation(), EnumParticle.FLAME, 0.5D, 0.0D, 0.5D, 0.0F, 5);

                        }

                        if (CostumeManager.equippedCostume(p).getName().equalsIgnoreCase("bruineruiter")) {

                            ParticleUtil.sendToLocation(p.getLocation(), EnumParticle.BLOCK_CRACK, 0.5D, 0.0D, 0.5D, 0.0F, 5, 3);

                        }

                        if (CostumeManager.equippedCostume(p).getName().equalsIgnoreCase("groeneruiter")) {

                            ParticleUtil.sendToLocation(p.getLocation(), EnumParticle.BLOCK_CRACK, 0.5D, 0.0D, 0.5D, 0.0F, 5, 5);

                        }

                        if (CostumeManager.equippedCostume(p).getName().equalsIgnoreCase("paarseruiter")) {

                            ParticleUtil.sendToLocation(p.getLocation(), EnumParticle.BLOCK_CRACK, 0.5D, 0.0D, 0.5D, 0.0F, 5, 42);

                        }

                        if (CostumeManager.equippedCostume(p).getName().equalsIgnoreCase("blauweruiter")) {

                            ParticleUtil.sendToLocation(p.getLocation(), EnumParticle.WATER_SPLASH, 0.5D, 0.0D, 0.5D, 0.0F, 5);

                        }

                    }

                }

            }

        }

    }

}


