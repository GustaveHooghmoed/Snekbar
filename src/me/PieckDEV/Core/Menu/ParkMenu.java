package me.PieckDEV.Core.Menu;


import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Utils.ItemStackUtil;
import me.PieckDEV.Core.Utils.MenuUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class ParkMenu {

    public static Inventory parkmenu(Player p) {

        Inventory inv = MenuUtil.menu(MS.c("           &8&nPieckCraft"), 2);


        ItemStack rides = new ItemStack(Material.MINECART);

        String[] rideslore = {"&7Klik hier om een lijst", "&7te openen met al onze", "&7attracties en hun status!"};

        cc(rideslore, rides);

        ItemStackUtil.setName(rides, c("&3Attracties"));

        inv.setItem(0, rides);


        ItemStack costumes = new ItemStack(Material.LEATHER_CHESTPLATE);

        String[] costumeslore = {"&7Klik hier om een lijst", "&7te openen met jouw kostuums!"};

        cc(costumeslore, costumes);

        ItemStackUtil.setName(costumes, c("&3Kostuums"));

        inv.setItem(2, costumes);


        ItemStack rooms = new ItemStack(Material.CHEST);

        String[] roomslore = {"&7Klik hier om een lijst", "&7te openen met al jouw boekingen!"};

        cc(roomslore, rooms);

        ItemStackUtil.setName(rooms, c("&3Boekingen"));

        inv.setItem(4, rooms);


        ItemStack rules = new ItemStack(Material.BOOK_AND_QUILL);

        String[] ruleslore = {"&7Klik hier om een lijst", "&7te openen met al onze &7regels!"};

        cc(ruleslore, rules);

        ItemStackUtil.setName(rules, c("&3Regels"));

        inv.setItem(6, rules);


        ItemStack achievements = new ItemStack(Material.NETHER_STAR);

        String[] achievementslore = {"&7Klik hier om een lijst", "&7te openen met al onze", "&7achievements!"};

        cc(achievementslore, achievements);

        ItemStackUtil.setName(achievements, c("&3Achievements"));

        inv.setItem(8, achievements);


        ItemStack fotos = new ItemStack(Material.ITEM_FRAME);

        String[] fotoslore = {"&7Klik hier om jouw foto's ", "&7te bekijken!"};

        cc(fotoslore, fotos);

        ItemStackUtil.setName(fotos, c("&3Foto Galerij"));

        inv.setItem(10, fotos);


        ItemStack muziek = new ItemStack(Material.JUKEBOX);

        String[] muzieklore = {"&7Klik hier om een keuze", "&7te maken tussen de oude", "&7en de nieuwe parkmuziek!"};

        cc(muzieklore, muziek);

        ItemStackUtil.setName(muziek, c("&3Parkmuziek"));

        inv.setItem(12, muziek);


        ItemStack shows = new ItemStack(Material.PAPER);

        String[] showslore = {"&7Klik hier om onze shows ", "&7en tijden te zien!"};

        cc(showslore, shows);

        ItemStackUtil.setName(shows, c("&3Shows & Tijden"));

        inv.setItem(14, shows);


        ItemStack shop = new ItemStack(175, 1);

        String[] shoplore = {"&7Klik hier om webshop ", "&7te openen!"};

        cc(shoplore, shop);

        ItemStackUtil.setName(shop, c("&3Shop"));

        inv.setItem(16, shop);


        return inv;

    }


    private static String c(String string) {

        return MS.c(string);

    }


    static void cc(String[] ruleslore, ItemStack rides) {

        ItemMeta im = rides.getItemMeta();

        im.setLore(MS.cc(ruleslore));

        rides.setItemMeta(im);

    }


    public static ItemStack parkmenu() {

        ItemStack is = new ItemStack(Material.ENCHANTED_BOOK);

        String[] lore = {"", "&7In dit menu kan je verschillende", "&7onderdelen terugvinden zoals", "&7attractie status, kostuums, ..."};


        ItemStackUtil.setName(is, c("&3EftelMenu &7(Rechts klikken)"));

        cc(lore, is);


        return is;

    }

}


