package me.PieckDEV.Core.Utils;

import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.List;

public class BlockUtil {

    public static List<Block> getNearbyBlocks(Block start, List<Material> allowedMaterials, List<Block> blocks) {
        for (int x = -1; x < 2; x++) {
            for (int y = -1; y < 2; y++) {
                for (int z = -1; z < 2; z++) {
                    Block block = start.getLocation().clone().add(x, y, z).getBlock();
                    if (block != null && !blocks.contains(block) && allowedMaterials.contains(block.getType())) {
                        blocks.add(block);
                        blocks.addAll(getNearbyBlocks(block, allowedMaterials, blocks));
                    }
                }
            }
        }
        return blocks;
    }
}
