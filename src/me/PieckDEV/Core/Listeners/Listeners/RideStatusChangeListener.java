package me.PieckDEV.Core.Listeners.Listeners;


import me.CaptainXan.PardoesCraft.Managers.RideManager;
import me.CaptainXan.PardoesCraft.Rides.Enums.RideStatus;
import me.CaptainXan.PardoesCraft.Rides.Events.RideStatusChangeEvent;
import me.CaptainXan.PardoesCraft.Rides.Ride;
import me.CaptainXan.PardoesCraft.Utils.RawMessage.RawMessages;
import me.IkeaKasten.Utils.MS;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;


public class RideStatusChangeListener
        implements Listener {

    @EventHandler
    public void onRideStatusChange(RideStatusChangeEvent e) {

        Ride ride = e.getRide();

        RideStatus status = e.getStatus();

        if (status == RideStatus.OPEN) {

            for (Player p : Bukkit.getOnlinePlayers()) {

                RawMessages.sendRawMessage(p, c("&3Attractie &8& &7" + ride.getTitle() + " is nu geopend! "), c("&8[&cKlik &chier&8]"),
                        c("&7Klik hier om te warpen naar deze attractie"), c("/warp " + ride.getName()));

            }

        }

        if (status == RideStatus.CLOSED) {

            Bukkit.broadcastMessage(c("&3Attractie &8& &7" + ride.getTitle() + " is nu gesloten!"));

        }

        RideManager.setStatusInFile(ride, status);

    }


    private String c(String string) {

        return MS.c(string);

    }

}


