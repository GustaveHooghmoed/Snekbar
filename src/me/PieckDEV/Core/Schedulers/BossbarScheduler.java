package me.PieckDEV.Core.Schedulers;


import me.IkeaKasten.Utils.MS;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;


public class BossbarScheduler
        extends BukkitRunnable {
    public static BossBar bar = null;
    public static List<String> messages = new ArrayList();


    public void run() {

        if (bar != null) {

            String next = messages.get(getMessage());


            bar.setTitle(next);

            bar.setProgress((messages.indexOf(next) + 1) / messages.size());

        } else {

            BossBar bar = Bukkit.createBossBar(MS.c("&c&7"), BarColor.RED, BarStyle.SOLID);

            bar = bar;

            messages.add(MS.c("&7Welkom in!"));

            messages.add(MS.c("&c&lNIEUW! &7Je kan nu je eigen ballon samenstellen!"));

            messages.add(MS.c("&7Gebruik &c/audio &7om te verbinden met onze audioserver!"));

            messages.add(MS.c("&7Wil je &cspeciale &7mogelijkheden? Koop medewerker in onze webshop!"));

            messages.add(MS.c("&7Gebruik het &cpark menu &7voor attractie statussen, achievements, ..."));

            messages.add(MS.c("&c&lNIEUW! &7Je kan nu je de parkmuziek instellen!"));


            bar.setTitle(messages.get(0));

            for (Player p : Bukkit.getOnlinePlayers()) {

                if (!bar.getPlayers().contains(p)) {

                    bar.addPlayer(p);

                }

            }

        }

    }


    public void disable() {

        for (Player p : bar.getPlayers()) {

            bar.removePlayer(p);

        }

    }


    public int getMessage() {

        int message = 0;

        if (messages.indexOf(bar.getTitle()) != messages.size() - 1) {

            message = messages.indexOf(bar.getTitle()) + 1;

        } else {

            return 0;

        }

        return message;

    }

}


