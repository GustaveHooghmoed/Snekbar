package me.PieckDEV.Core.Listeners.Listeners;


import me.CaptainXan.PardoesCraft.Utils.Guests.Guest;
import me.CaptainXan.PardoesCraft.Utils.NameUtil;
import me.CaptainXan.PardoesCraft.Utils.StringUtil;
import me.IkeaKasten.Utils.MS;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import ru.tehkode.permissions.PermissionEntity;
import ru.tehkode.permissions.events.PermissionEntityEvent;


public class PermissionEntityListener
        implements Listener {

    @EventHandler
    public void onPermissions(PermissionEntityEvent e) {

        PermissionEntity pe = e.getEntity();

        Player p = Bukkit.getPlayer(pe.getName());

        Guest g = new Guest(p);

        if (e.getAction() == PermissionEntityEvent.Action.INHERITANCE_CHANGED) {

            Bukkit.broadcast(MS.c("&6" + StringUtil.get3rdPersonName(p.getName()) + " &egroup has been set to &6" + g.getGroup().getName().toLowerCase() + "&e!"), "PC.Admin");


            NameUtil.setPrefix(p, ChatColor.translateAlternateColorCodes('&', g.getGroup().getPrefix()));

            NameUtil.setTabName(p, ChatColor.translateAlternateColorCodes('&', g.getGroup().getOwnPrefix().substring(0, 2)) + p.getName());

        }

    }

}


