package me.PieckDEV.Core.Utils;

import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;

public class TrackUtil {

    public static List<Block> getTrack(Block start, List<Material> allowedMaterials) {
        return BlockUtil.getNearbyBlocks(start, allowedMaterials, new ArrayList<Block>());
    }
}
