package me.PieckDEV.Core.Utils;


import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;


public class ParticleUtil {
    public static int default_distance = 200;


    public static void sendToLocation(Location loc, EnumParticle type, double xd, double yd, double zd, float speed, int count) {

        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(type, true, (float) loc.getX(), (float) loc.getY(), (float) loc.getZ(), (float) xd, (float) yd, (float) zd, speed, count, default_distance);

        for (Player p : Bukkit.getOnlinePlayers()) {

            CraftPlayer cp = (CraftPlayer) p;

            cp.getHandle().playerConnection.networkManager.sendPacket(packet);

        }

    }


    public static void sendToLocation(Location loc, EnumParticle type, double xd, double yd, double zd, float speed, int count, int extra) {

        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(type, true, (float) loc.getX(), (float) loc.getY(), (float) loc.getZ(), (float) xd, (float) yd, (float) zd, speed, count, extra, default_distance);

        for (Player p : Bukkit.getOnlinePlayers()) {

            CraftPlayer cp = (CraftPlayer) p;

            cp.getHandle().playerConnection.networkManager.sendPacket(packet);

        }

    }


    public static void sendToPlayer(Player p, Location loc, EnumParticle type, double xd, double yd, double zd, float speed, int count) {

        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(type, true, (float) loc.getX(), (float) loc.getY(), (float) loc.getZ(), (float) xd, (float) yd, (float) zd, speed, count, default_distance);


        CraftPlayer cp = (CraftPlayer) p;

        cp.getHandle().playerConnection.networkManager.sendPacket(packet);

    }

}


