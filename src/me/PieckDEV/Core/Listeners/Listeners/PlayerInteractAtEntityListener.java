package me.PieckDEV.Core.Listeners.Listeners;


import me.CaptainXan.PardoesCraft.Managers.AchievementManager;
import me.CaptainXan.PardoesCraft.Managers.CostumeManager;
import me.CaptainXan.PardoesCraft.Menu.CostumeMenu;
import me.CaptainXan.PardoesCraft.Utils.Guests.Guest;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;


public class PlayerInteractAtEntityListener
        implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent e) {

        Player p = e.getPlayer();

        Entity ent = e.getRightClicked();

        if ((ent instanceof Player)) {

            Player p2 = (Player) ent;

            Guest g = new Guest(p2);

            if (g.getGroup().getName().equalsIgnoreCase("owner")) {

                AchievementManager.giveAchievement(p, AchievementManager.getAchievementFromName("directie"));

            }

        }

        if ((ent instanceof ArmorStand)) {

            ArmorStand stand = (ArmorStand) ent;

            if (stand.getCustomName() != null) {

                if ((stand.getCustomName().equalsIgnoreCase("MCCup")) &&
                        (stand.getPassenger() == null)) {

                    stand.setPassenger(p);

                }

                if (stand.getCustomName().contains("costume:")) {

                    e.setCancelled(true);


                    Costume c = CostumeManager.getCostumeFromName(stand.getCustomName().replace("costume:", ""));

                    if (c != null) {

                        p.openInventory(CostumeMenu.shopcostumemenu(p, c));

                    }

                }

            }

        }

    }

}


