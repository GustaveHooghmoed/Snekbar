package me.PieckDEV.Core.Utils.Relatives;


import me.PieckDEV.Core.Utils.LocationUtil;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;

import java.util.HashMap;


public class RelativeUtil {
    public static HashMap<ArmorStand, StandRelative> relatives = new HashMap();
    public static HashMap<ArmorStand, StandRelative2> relatives2 = new HashMap();


    public static void updateRelative(StandRelative relative) {

        Location to = LocationUtil.getRelative(relative.getCenterStand().getLocation(), relative.getDistance(), relative.getCenterStand().getLocation().getYaw() + relative.getAngle());

        to.setY(relative.getCenterStand().getLocation().getY() - 0.5D);

        relative.getSecondaryStand().setGravity(false);


        CraftEntity ent = (CraftEntity) relative.getSecondaryStand();

        ent.getHandle().setPosition(to.getX(), to.getY(), to.getZ());

    }


    public static void updateRelative1(StandRelative relative) {

        Location to = LocationUtil.getRelative(relative.getCenterStand().getLocation(), relative.getDistance(), relative.getCenterStand().getLocation().getYaw() + relative.getAngle() + 85.94366926962348D);

        to.setY(relative.getCenterStand().getLocation().getY() - 0.5D);

        relative.getSecondaryStand().setGravity(false);


        CraftEntity ent = (CraftEntity) relative.getSecondaryStand();

        ent.getHandle().setPosition(to.getX(), to.getY(), to.getZ());

    }

}


