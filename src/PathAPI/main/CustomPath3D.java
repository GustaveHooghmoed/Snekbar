package PathAPI.main;


import me.CaptainXan.PardoesCraft.Main.Main;
import me.CaptainXan.PardoesCraft.Utils.Objects.Spline3D;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import toxi.geom.Vec3D;

import java.util.ArrayList;
import java.util.List;


public class CustomPath3D {
    private ArrayList<Location> points = new ArrayList();
    private ArrayList<Location> locations = new ArrayList();
    private Spline3D spline = new Spline3D();


    public CustomPath3D() {

        Bukkit.getLogger().info("Created a new Custom-3D path!");

    }


    public ArrayList<Location> getPoints() {

        return this.points;

    }


    public void addPoint(Location point) {

        if (!this.points.contains(point)) {

            this.points.add(point);

        }

    }


    public void removePoint(Location point) {

        if (this.points.contains(point)) {

            this.points.remove(point);

        }

    }


    public ArrayList<Location> getLocations() {

        return this.locations;

    }


    public void calculate() {
        toxi.geom.Spline3D spline3D = new toxi.geom.Spline3D();
        for (Location loc : points) {
            spline3D.add(new Vec3D((float)loc.getX(),(float)loc.getY(),(float)loc.getZ()));
        }

        List<Vec3D> list = spline3D.getDecimatedVertices(0.05f);
        for (Vec3D vec3D : list) {
            this.locations.add(new Location(Main.getWorld(), vec3D.x,vec3D.y,vec3D.z));
        }
    }

}


