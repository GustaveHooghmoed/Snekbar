package PathAPI.main;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;


public abstract class BasicSpline {
    public static final Object[] EMPTYOBJLIST = new Object[0];


    public void calcNaturalCubic(List<?> valueCollection, Method getVal, Collection<Cubic> cubicCollection)
            throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        int num = valueCollection.size() - 1;


        float[] gamma = new float[num + 1];

        float[] delta = new float[num + 1];

        float[] D = new float[num + 1];


        gamma[0] = 0.5F;

        for (int i = 1; i < num; i++) {

            gamma[i] = (1.0F / (4.0F - gamma[(i - 1)]));

        }

        gamma[num] = (1.0F / (2.0F - gamma[(num - 1)]));


        Float p0 = (Float) getVal.invoke(valueCollection.get(0), EMPTYOBJLIST);

        Float p1 = (Float) getVal.invoke(valueCollection.get(1), EMPTYOBJLIST);


        delta[0] = (3.0F * (p1.floatValue() - p0.floatValue()) * gamma[0]);


        for (int i = 1; i < num; i++) {

            p0 = (Float) getVal.invoke(valueCollection.get(i - 1), EMPTYOBJLIST);

            p1 = (Float) getVal.invoke(valueCollection.get(i + 1), EMPTYOBJLIST);

            delta[i] = ((3.0F * (p1.floatValue() - p0.floatValue()) - delta[(i - 1)]) * gamma[i]);

            p0 = (Float) getVal.invoke(valueCollection.get(num - 1), EMPTYOBJLIST);

            p1 = (Float) getVal.invoke(valueCollection.get(num), EMPTYOBJLIST);


            delta[num] = ((3.0F * (p1.floatValue() - p0.floatValue()) - delta[(num - 1)]) * gamma[num]);


            D[num] = delta[num];

            cubicCollection.clear();

            for (i = 0; i < num; i++) {

                p0 = (Float) getVal.invoke(valueCollection.get(i), EMPTYOBJLIST);

                p1 = (Float) getVal.invoke(valueCollection.get(i + 1), EMPTYOBJLIST);


                cubicCollection.add(new Cubic(
                        p0.floatValue(),
                        D[i],
                        3.0F * (p1.floatValue() - p0.floatValue()) - 2.0F * D[i] - D[(i + 1)],
                        2.0F * (p0.floatValue() - p1.floatValue()) + D[i] + D[(i + 1)]));

            }

        }

    }
}


