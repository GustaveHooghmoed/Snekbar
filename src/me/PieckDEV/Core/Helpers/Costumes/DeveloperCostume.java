package me.PieckDEV.Core.Helpers.Costumes;


import me.CaptainXan.PardoesCraft.Utils.Enums.CostumeType;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import me.CaptainXan.PardoesCraft.Utils.ItemStackUtil;
import me.CaptainXan.PardoesCraft.Utils.SkullUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class DeveloperCostume
        implements Costume {

    public String getName() {

        return "developer";

    }


    public String getTitle() {

        return "Developer";

    }


    public ItemStack getHelmet() {

        // ItemStack is = SkullUtil.getSkull("http://textures.minecraft.net/texture/8d19c68461666aacd7628e34a1e2ad39fe4f2bde32e231963ef3b35533");
        ItemStack is = SkullUtil.getSkull("http://textures.minecraft.net/texture/f9aa17fa592c7c854faa64bbe9b0f3bf1bfe6b2d74278b1965906e188f33d2e");
        return is;

    }


    public ItemStack getChestplate() {

        ItemStack is = new ItemStack(Material.LEATHER_CHESTPLATE);

        ItemStackUtil.setLeatherRGB(is, 194, 198, 206);

        return is;

    }


    public ItemStack getLeggings() {

        ItemStack is = new ItemStack(Material.LEATHER_LEGGINGS);

        ItemStackUtil.setLeatherRGB(is, 26, 27, 28);

        return is;

    }


    public ItemStack getBoots() {

        ItemStack is = new ItemStack(Material.LEATHER_BOOTS);

        ItemStackUtil.setLeatherRGB(is, 194, 198, 206);

        return is;

    }


    public CostumeType getType() {

        return CostumeType.VIP;

    }


    public int getPrice() {

        return Integer.MAX_VALUE;

    }

}


