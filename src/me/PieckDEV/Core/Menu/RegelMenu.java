package me.PieckDEV.Core.Menu;


import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Utils.ItemStackUtil;
import me.PieckDEV.Core.Utils.MenuUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


public class RegelMenu {

    public static Inventory RegelMenu(Player p) {

        Inventory inv = MenuUtil.menu(MS.c("                &8&nRegels"), 1);

        ItemStack regel1 = new ItemStack(Material.PAPER);

        ItemStackUtil.setName(regel1, MS.c("&7Ben aardig tegenover anderen."));

        inv.setItem(0, regel1);


        ItemStack regel2 = new ItemStack(Material.PAPER);

        ItemStackUtil.setName(regel2, MS.c("&7Let op je taalgebruik."));

        inv.setItem(1, regel2);


        ItemStack regel3 = new ItemStack(Material.PAPER);

        ItemStackUtil.setName(regel3, MS.c("&7Sla Michel als het kan."));

        inv.setItem(2, regel3);


        ItemStack regel4 = new ItemStack(Material.PAPER);

        ItemStackUtil.setName(regel4, MS.c("&7Respecteer elkaar."));

        inv.setItem(3, regel4);


        return inv;

    }

}


