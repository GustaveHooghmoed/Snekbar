package me.PieckDEV.Core.Managers;


import me.CaptainXan.PardoesCraft.Rides.Enums.Realm;
import me.CaptainXan.PardoesCraft.Rides.Enums.RideStatus;
import me.CaptainXan.PardoesCraft.Rides.Enums.RideType;
import me.CaptainXan.PardoesCraft.Rides.Ride;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class RideManager {
    public static ArrayList<Ride> rides = new ArrayList();


    public static void setup() {

        rides.add(new Ride("sym","Symbolica", RideType.DARKRIDE, Realm.FANTASIERIJK));

        rides.add(new Ride("jedd", "Joris en de Draak", RideType.ROLLERCOASTER, Realm.RUIGRIJK));

        rides.add(new Ride("baron", "Baron 1898", RideType.DIVECOASTER, Realm.RUIGRIJK));

        rides.add(new Ride("droomvlucht", "Droomvlucht", RideType.DARKRIDE, Realm.MARERIJK));

        rides.add(new Ride("dvh", "De Vliegende Hollander", RideType.DARKRIDE, Realm.RUIGRIJK));

        rides.add(new Ride("pirana", "Piraña", RideType.WATERRIDE, Realm.ANDERRIJK));

        rides.add(new Ride("python", "Python", RideType.ROLLERCOASTER, Realm.RUIGRIJK));

        rides.add(new Ride("cf", "Carnaval Festival", RideType.DARKRIDE, Realm.REIZENRIJK));

        rides.add(new Ride("stcarrousel", "Stoomcarrousel", RideType.FLATRIDE, Realm.MARERIJK));

        rides.add(new Ride("fm", "Fata Morgana", RideType.DARKRIDE, Realm.ANDERRIJK));

        rides.add(new Ride("vogelrok", "Vogel Rok", RideType.DARKRIDE, Realm.REIZENRIJK));

        rides.add(new Ride("villavolta", "Villa Volta", RideType.MADHOUSE, Realm.MARERIJK));

        rides.add(new Ride("bobslee", "Bob", RideType.BOB_SLED, Realm.ANDERRIJK));

        rides.add(new Ride("hm", "Halve Maen", RideType.FLATRIDE, Realm.RUIGRIJK));

        rides.add(new Ride("dot", "De Oude Tufferbaan", RideType.FLATRIDE, Realm.RUIGRIJK));

        rides.add(new Ride("pd", "PandaDroom", RideType.SHOW, Realm.ANDERRIJK));

        rides.add(new Ride("symbolica", "Symbolica", RideType.DARKRIDE, Realm.MARERIJK));

        rides.add(new Ride("monorail", "Monorail", RideType.MONORAIL, Realm.MARERIJK));

        rides.add(new Ride("mc", "Monsieur Cannibale", RideType.FLATRIDE, Realm.REIZENRIJK));

        rides.add(new Ride("pagode", "Pagode", RideType.FLATRIDE, Realm.REIZENRIJK));

        rides.add(new Ride("gondoletta", "Gondoletta", RideType.MISC, Realm.REIZENRIJK));

        rides.add(new Ride("doolhof", "Avonturendoolhof", RideType.MISC, Realm.REIZENRIJK));

        rides.add(new Ride("polka", "Polka Marina", RideType.FLATRIDE, Realm.RUIGRIJK));

        rides.add(new Ride("spookslot", "Spookslot", RideType.SHOW, Realm.ANDERRIJK));

        rides.add(new Ride("kinderspoor", "Kinderspoor", RideType.MISC, Realm.RUIGRIJK));


        setFile();

        updateRideStatus();

    }


    public static Ride getRideFromTitle(String title) {

        if (rideExistsTitle(title)) {

            for (Ride r : rides) {

                if (r.getTitle().equalsIgnoreCase(title)) {

                    return r;

                }

            }

        }

        return null;

    }


    public static Ride getRideFromAlias(String alias) {

        if (rideExistsAlias(alias)) {

            for (Ride r : rides) {

                if (r.getName().equalsIgnoreCase(alias)) {

                    return r;

                }

            }

        }

        return null;

    }


    public static boolean rideExistsTitle(String title) {

        for (Ride r : rides) {

            if (r.getTitle().equalsIgnoreCase(title)) {

                return true;

            }

        }

        return false;

    }


    public static boolean rideExistsAlias(String alias) {

        for (Ride r : rides) {

            if (r.getName().equalsIgnoreCase(alias)) {

                return true;

            }

        }

        return false;

    }


    public static ArrayList<Ride> sortRidesByRealm(Realm realm) {

        ArrayList<Ride> list = new ArrayList();

        for (Ride ride : sortByStatus(rides)) {

            if (ride.getRealm() == realm) {

                list.add(ride);

            }

        }

        return list;

    }


    public static ArrayList<Ride> sortByStatus(ArrayList<Ride> rides) {

        ArrayList<Ride> list = new ArrayList();

        ArrayList<Ride> open = new ArrayList();

        ArrayList<Ride> closed = new ArrayList();

        ArrayList<Ride> maintenance = new ArrayList();

        for (Ride ride : rides) {

            if (ride.getStatus() == RideStatus.OPEN) {

                open.add(ride);

            }

        }

        for (Ride ride : rides) {

            if (ride.getStatus() == RideStatus.CLOSED) {

                closed.add(ride);

            }

        }

        for (Ride ride : rides) {

            if (ride.getStatus() == RideStatus.MAINTENANCE) {

                maintenance.add(ride);

            }

        }

        list.addAll(open);

        list.addAll(closed);

        list.addAll(maintenance);


        return list;

    }


    public static void updateRideStatus() {

        for (Ride ride : rides) {

            ride.setStatus(getStatusFromFile(ride));

        }

    }


    public static RideStatus getStatusFromFile(Ride ride) {

        YamlConfiguration conf = YamlConfiguration.loadConfiguration(getFile());

        if (conf.get(ride.getName()) != null) {

            if (conf.getString(ride.getName()).equalsIgnoreCase("open")) {

                return RideStatus.OPEN;

            }

            if (conf.getString(ride.getName()).equalsIgnoreCase("closed")) {

                return RideStatus.CLOSED;

            }

            if (conf.getString(ride.getName()).equalsIgnoreCase("maintenance")) {

                return RideStatus.MAINTENANCE;

            }

        }

        return RideStatus.CLOSED;

    }


    public static void setStatusInFile(Ride ride, RideStatus status) {

        YamlConfiguration conf = YamlConfiguration.loadConfiguration(getFile());


        conf.set(ride.getName(), status.toString());

        try {

            conf.save(getFile());

        } catch (IOException e) {

            e.printStackTrace();

        }

    }


    public static void setFile() {

        File f = new File("plugins/PardoesCraft/ridedata/attracties.yml");

        if (!f.exists()) {

            YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

            for (Ride ride : rides) {

                conf.set(ride.getName(), ride.getStatus().toString());

            }

            try {

                conf.save(f);

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

    }


    public static boolean fileExists() {

        File f = new File("plugins/PardoesCraft/ridedata/attracties.yml");


        return f.exists();

    }


    public static File getFile() {

        return new File("plugins/PardoesCraft/ridedata/attracties.yml");

    }

}


