package me.PieckDEV.Core.Rides.TrackTest;

import PathAPI.main.CustomPath3D;
import me.PieckDEV.Core.Main.Main;
import me.PieckDEV.Core.Utils.LocationUtil;
import me.PieckDEV.Core.Utils.TrackUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class Track {

    public final List<Block> blocks;

    public final ArrayList<Material> allowed = new ArrayList<>();
    public ArrayList<Location> locs;

    public Track() {
        Location location = LocationUtil.getLocationFromCoords(650,69,128);
        allowed.add(Material.EMERALD_BLOCK);
        allowed.add(Material.CONCRETE);
        this.blocks = TrackUtil.getTrack(location.getBlock(),allowed);
        Bukkit.broadcastMessage("size: " + this.blocks.size());
        CustomPath3D path3D = new CustomPath3D();
        for (Block b : blocks){
            if (b.getType() == Material.EMERALD_BLOCK){
            path3D.addPoint(b.getLocation()); }

        }
        path3D.calculate();
        this.locs = path3D.getLocations();
    }

public void exec() {

    ArmorStand as = this.locs.get(0).getWorld().spawn(this.locs.get(0),ArmorStand.class);
        as.setGravity(false);
        new BukkitRunnable(){
        public int timer = 0;
        @Override
        public void run() {
            this.timer +=4;
            Bukkit.broadcastMessage("size: " + timer );
            if (timer < locs.size()){

                as.teleport(locs.get(this.timer));
            } else {
                cancel();
                as.remove();
            }
        }
    }.runTaskTimer(Main.getPl(),0l,0l);
}

}