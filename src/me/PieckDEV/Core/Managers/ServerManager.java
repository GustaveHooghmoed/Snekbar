package me.PieckDEV.Core.Managers;

import me.CaptainXan.PardoesCraft.Helpers.Messages;
import me.CaptainXan.PardoesCraft.Utils.Enums.Server.ActingServerType;
import org.bukkit.Bukkit;

public class ServerManager {

    private static ActingServerType acting = ActingServerType.ONDERHOUD;

    public ServerManager() {
        acting = ActingServerType.ONDERHOUD;
    }

    public static String[] getWhitelistMessage(){
        if (getActing() == ActingServerType.ONDERHOUD) {
            return Messages.Whitelist();
        } else {
            return null;
        }
    }

    public static ActingServerType getActing() {
        return acting;
    }

    public static void setActing(ActingServerType acting) {
        ServerManager.acting = acting;
    }

    public void shutdown() {
        Bukkit.getServer().shutdown();
    }
}
