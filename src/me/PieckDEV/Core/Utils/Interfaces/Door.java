package me.PieckDEV.Core.Utils.Interfaces;

public interface Door {
    void spawn();

    void despawn();

    void open();

    void close();

    String name();
}


