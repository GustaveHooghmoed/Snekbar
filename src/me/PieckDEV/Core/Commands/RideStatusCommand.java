/*    */
package me.PieckDEV.Core.Commands;
/*    */ 
/*    */

import me.CaptainXan.PardoesCraft.Rides.Enums.RideStatus;
import me.CaptainXan.PardoesCraft.Rides.Ride;
import me.CaptainXan.PardoesCraft.Utils.MessageUtil;
import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Managers.RideManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */

/*    */
/*    */ 
/*    */ public class RideStatusCommand
/*    */ implements CommandExecutor
/*    */ {
    /* 18 */   public String[] args0 = {"open", "closed", "maintenance"};

    /*    */
/*    */
    public boolean onCommand(CommandSender sender, Command cmd, String name, String[] args)
/*    */ {
/* 22 */
        if (cmd.getName().equalsIgnoreCase("status")) {
/* 23 */
            if (sender.isOp())
/*    */ {
/* 25 */
                if (args.length > 1)
/*    */ {
/* 27 */
                    if (Arrays.asList(this.args0).contains(args[0]))
/*    */ {
/* 29 */
                        Ride ride = RideManager.getRideFromAlias(args[1]);
/* 30 */
                        if (ride != null) {
/* 31 */
                            ride.setStatus(RideStatus.valueOf(args[0].toUpperCase()));
/*    */
                        } else {
/* 33 */
                            sender.sendMessage(MS.c("&cEr is geen attractie gevonden met de naam '" + args[1] + "'!"));
/*    */
                        }
/*    */
                    }
/*    */
                    else
/*    */ {
/* 38 */
                        sender.sendMessage(MS.c("&4Gebruik: &c/status <open|closed|maintenance> <ride alias>"));
/*    */
                    }
/*    */
                }
/*    */
                else {
/* 42 */
                    sender.sendMessage(MS.c("&4Gebruik: &c/status <open|closed|maintenance> <ride alias>"));
/*    */
                }
/*    */
            }
/*    */
            else {
/* 46 */
                sender.sendMessage(MessageUtil.noPermissionForCmd());
/*    */
            }
/*    */
        }
/* 49 */
        return false;
/*    */
    }
/*    */
}


/* Location:              C:\Users\Zorgklas 2\Desktop\PardoesCraft\plugins\PardoesCraft.jar!\me\CaptainXan\PardoesCraft\Commands\RideStatusCommand.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */