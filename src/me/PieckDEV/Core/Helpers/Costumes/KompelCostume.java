package me.PieckDEV.Core.Helpers.Costumes;


import me.CaptainXan.PardoesCraft.Utils.Enums.CostumeType;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import me.CaptainXan.PardoesCraft.Utils.ItemStackUtil;
import me.CaptainXan.PardoesCraft.Utils.SkullUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class KompelCostume
        implements Costume {

    public String getName() {

        return "kompel";

    }


    public String getTitle() {

        return "Kompel";

    }


    public ItemStack getHelmet() {

        ItemStack is = SkullUtil.getSkull("http://textures.minecraft.net/texture/d357444ade64ec6cea645ec57e775864d67c5fa62299786e03799317ee4ad");

        return is;

    }


    public ItemStack getChestplate() {

        ItemStack is = new ItemStack(Material.LEATHER_CHESTPLATE);

        ItemStackUtil.setLeatherRGB(is, 153, 204, 255);

        return is;

    }


    public ItemStack getLeggings() {

        ItemStack is = new ItemStack(Material.LEATHER_LEGGINGS);

        ItemStackUtil.setLeatherRGB(is, 255, 255, 0);

        return is;

    }


    public ItemStack getBoots() {

        ItemStack is = new ItemStack(Material.LEATHER_BOOTS);

        ItemStackUtil.setLeatherRGB(is, 32, 32, 32);

        return is;

    }


    public CostumeType getType() {

        return CostumeType.GENERAL;

    }


    public int getPrice() {

        return 2500;

    }

}


