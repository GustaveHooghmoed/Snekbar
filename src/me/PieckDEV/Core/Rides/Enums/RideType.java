package me.PieckDEV.Core.Rides.Enums;

public enum RideType {
    DARKRIDE, WATERRIDE, ROLLERCOASTER, DIVECOASTER, FLATRIDE, BOB_SLED, MADHOUSE, SHOW, MONORAIL, MISC
}


