package me.PieckDEV.Core.Utils.Objects;


public class Achievement {
    private String name;
    private String title;
    private String[] uncompDesc;
    private String[] compDesc;
    private int reward;


    public Achievement(String name, String title, String[] uncompDesc, String[] compDesc, int reward) {

        this.name = name;

        this.title = title;

        this.uncompDesc = uncompDesc;

        this.compDesc = compDesc;

        this.reward = reward;

    }


    public String getName() {

        return this.name;

    }


    public String getTitle() {

        return this.title;

    }


    public String[] getUncompletedDescription() {

        return this.uncompDesc;

    }


    public String[] getCompletedDescription() {

        return this.compDesc;

    }


    public int getReward() {

        return this.reward;

    }

}


