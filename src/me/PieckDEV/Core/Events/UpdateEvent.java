package me.PieckDEV.Core.Events;
//test
import me.CaptainXan.PardoesCraft.Utils.Enums.UpdateType;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class UpdateEvent
        extends Event {
    private static final HandlerList handlers = new HandlerList();
    private UpdateType type;


    public UpdateEvent(UpdateType type) {

        this.type = type;

    }


    public static HandlerList getHandlerList() {

        return handlers;

    }


    public UpdateType getType() {

        return this.type;

    }


    public HandlerList getHandlers() {

        return handlers;

    }

}


