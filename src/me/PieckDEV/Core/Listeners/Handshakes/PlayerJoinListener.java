package me.PieckDEV.Core.Listeners.Handshakes;


import me.CaptainXan.PardoesCraft.Utils.Guests.Guest;
import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Main.Main;
import me.PieckDEV.Core.Managers.AchievementManager;
import me.PieckDEV.Core.Managers.CostumeManager;
import me.PieckDEV.Core.Menu.ParkMenu;
import me.PieckDEV.Core.Utils.NameUtil;
import me.PieckDEV.Core.Utils.ResourcepackUtil;
import me.PieckDEV.Core.Utils.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


public class PlayerJoinListener
        implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {

        final Player p = e.getPlayer();

        Guest g = new Guest(p);


        e.setJoinMessage(MS.c("&7&oWelkom " + p.getName() + "!"));

        p.getInventory().setItem(4, ParkMenu.parkmenu());


        String prefix = ChatColor.translateAlternateColorCodes('&', g.getGroup().getPrefix());

        NameUtil.setPrefix(p, prefix);

        NameUtil.setTabName(p, prefix.substring(0, 2) + p.getName());


        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.pl, new Runnable() {

            public void run() {

                StringUtil.sendEmptyMessage(p, 50);

                p.sendMessage(StringUtil.centerText(MS.c("&7Welkom in"), 75));

                p.sendMessage(StringUtil.centerText(MS.c("&c&lPieckCraft"), 70));

                StringUtil.sendEmptyMessage(p, 3);

                ResourcepackUtil.sendResourcepack(p);

            }

        }, 2L);


        AchievementManager.setFile(p);

        CostumeManager.setFile(p);

    }

}


