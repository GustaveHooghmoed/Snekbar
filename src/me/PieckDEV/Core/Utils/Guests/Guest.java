package me.PieckDEV.Core.Utils.Guests;


import me.PieckDEV.Core.Utils.EconomyUtil;
import me.PieckDEV.Core.Utils.ErrorUtil;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.bukkit.PermissionsEx;


public class Guest {
    private Player player;


    public Guest(Player player) {

        this.player = player;

    }


    public Player getPlayer() {

        return this.player;

    }


    public double getMoney() {
        double money = 0.0d;
        return EconomyUtil.getMoney(this.player);
    }


    public void giveMoney(int amount) {
       EconomyUtil.giveMoney(this.player,amount);

    }


    public void takeMoney(int amount) {

        EconomyUtil.takeMoney(this.player,amount);

    }


    public boolean hasRank() {

        return this.player.hasPermission("PC.Medewerker");

    }


    public String getOnlineTime() {

        long time = this.player.getStatistic(Statistic.PLAY_ONE_TICK) / 20;

        int sec = (int) (time % 60L);

        time /= 60L;

        int min = (int) (time % 60L);

        time /= 60L;

        int hours = (int) (time % 24L);

        time /= 24L;

        int days = (int) time;


        return days + "d" + hours + "u" + min + "m" + sec + "s";

    }


    public PermissionGroup getGroup() {

        try {

            return PermissionsEx.getPermissionManager().getUser(this.player).getGroups()[0];

        } catch (Exception e) {

            ErrorUtil.reportException(e, getClass());

        }

        return PermissionsEx.getPermissionManager().getGroup("default");

    }

}


