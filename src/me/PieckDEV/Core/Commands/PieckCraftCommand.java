package me.PieckDEV.Core.Commands;


import me.CaptainXan.AudioserverV2.Utils.ValidateUtil;
import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Managers.CostumeManager;
import me.PieckDEV.Core.Utils.MessageUtil;
import me.PieckDEV.Core.Utils.StringUtil;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;


public class PieckCraftCommand implements CommandExecutor {
    String[] args0 = { "reload", "addcostume", "cl", "audio"};


    public boolean onCommand(CommandSender sender, Command cmd, String name, String[] args) {

                if (cmd.getName().equalsIgnoreCase("pieckcraft")) {

            if (sender.isOp()) {

                if (args.length > 0) {

                    if (Arrays.asList(this.args0).contains(args[0])) {

                        if ((args[0].equalsIgnoreCase("addcostume")) &&
                                (args.length > 2) &&
                                (CostumeManager.getCostumeFromName(args[1]) != null) &&
                                (Bukkit.getPlayer(args[2]) != null) &&
                                (!CostumeManager.hasCostume(Bukkit.getPlayer(args[2]),
                                        CostumeManager.getCostumeFromName(args[1])))) {

                            CostumeManager.addCostume(Bukkit.getPlayer(args[2]),
                                    CostumeManager.getCostumeFromName(args[1]));

                            sender.sendMessage(
                                    c("&aSuccessfully added '" + CostumeManager.getCostumeFromName(args[1]).getTitle() +
                                            "' to " + StringUtil.get3rdPersonName(Bukkit.getPlayer(args[2]).getName())
                                            + " costume list!"));

                        }

                        if (args[0].equalsIgnoreCase("reload")) {

                            Bukkit.dispatchCommand(sender.getServer().getConsoleSender(),
                                    "plugman reload PieckCraft");

                        }
                        if (args[0].equalsIgnoreCase("audio")) {
                            Player p = (Player)sender;
                            Bukkit.broadcastMessage(ValidateUtil.generateKeyForPlayer(p));



                        }


                    } else {

                        sender.sendMessage("List of commands");

                    }

                } else {

                    sender.sendMessage("List of commands");

                }

            } else {

                sender.sendMessage(MessageUtil.noPermissionForCmd());

            }

        }

        return false;

    }


    private String c(String string) {

        return MS.c(string);

    }


}

