package me.PieckDEV.Core.Utils.Interfaces;

public interface Flatride {
    boolean isStarted();

    String getName();

    void spawn();

    void despawn();

    void start();
}


