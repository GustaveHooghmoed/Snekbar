package me.PieckDEV.Core.Listeners.Updates;


import me.CaptainXan.PardoesCraft.Utils.Guests.Guest;
import me.CaptainXan.PardoesCraft.Utils.Relatives.StandRelative;
import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Events.UpdateEvent;
import me.PieckDEV.Core.Managers.AchievementManager;
import me.PieckDEV.Core.Utils.EntityUtil;
import me.PieckDEV.Core.Utils.Enums.UpdateType;
import me.PieckDEV.Core.Utils.LocationUtil;
import me.PieckDEV.Core.Utils.TitleUtil;
import org.bukkit.Bukkit;
import org.bukkit.Statistic;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;


public class UpdateListener
        implements Listener {
    public StandRelative relative;


    @EventHandler
    public void onUpdate(UpdateEvent e) {

        UpdateType type = e.getType();


        if (type == UpdateType.TICK) {

            for (Entity ent : EntityUtil.getEntitiesByName("PardoesBeeld")) {

                LocationUtil.setM(ent, (float) (ent.getLocation().getYaw() + 0.2D));

            }

        }


        if (type == UpdateType.SECOND) {


            for (Player p : Bukkit.getOnlinePlayers()) {

                Guest g = new Guest(p);

                TitleUtil.setHeaderAndFooter(p, MS.c("&4Welkom in PieckCraft\n "), MS.c("\n&7Totale online tijd: " + g.getOnlineTime() + "\n&cserver.pieckcraft.nl"));


                long time = p.getStatistic(Statistic.PLAY_ONE_TICK) / 20;

                int sec = (int) (time % 60L);

                time /= 60L;

                int min = (int) (time % 60L);

                time /= 60L;

                int hours = (int) (time % 24L);

                time /= 24L;

                int days = (int) time;

                if (days >= 1) {

                    AchievementManager.giveAchievement(p, AchievementManager.getAchievementFromName("tweedethuis"));

                }

            }

        }

    }

}


