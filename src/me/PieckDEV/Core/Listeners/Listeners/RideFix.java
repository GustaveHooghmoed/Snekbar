package me.PieckDEV.Core.Listeners.Listeners;


import me.CaptainXan.PardoesCraft.Utils.LocationUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.spigotmc.event.entity.EntityDismountEvent;


public class RideFix
        implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {

        Player p = e.getPlayer();

        if (p.isInsideVehicle()) {

            p.eject();

            p.performCommand("warp " + LocationUtil.getNearestWarp(p));

            p.performCommand("warp " + LocationUtil.getNearestWarp(p));

        }

    }


    @EventHandler
    public void onEntityDismountEvent(EntityDismountEvent event) {

        Entity e = event.getEntity();

        Entity from = event.getDismounted();

        Bukkit.broadcastMessage(e.getType().toString());

        if (e.getType() == EntityType.PLAYER) {

            Player p = (Player) e;

            from.eject();

            p.performCommand("warp " + LocationUtil.getNearestWarp(p));

            p.performCommand("warp " + LocationUtil.getNearestWarp(p));

        }

    }


    @EventHandler
    public void onChunkLoad(ChunkUnloadEvent event) {

        if (event.getChunk().getEntities() != null) {
            Entity[] arrayOfEntity;

            int j = (arrayOfEntity = event.getChunk().getEntities()).length;
            for (int i = 0; i < j; i++) {
                Entity entity = arrayOfEntity[i];

                if ((entity instanceof ArmorStand)) {


                    if (entity.getCustomName().equalsIgnoreCase("MCCup")) {

                        event.setCancelled(true);

                        event.setSaveChunk(true);

                    }

                }

            }

        }

    }

}


