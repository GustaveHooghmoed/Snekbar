package me.PieckDEV.Core.Utils;


import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;


public class TitleUtil {

    public static void sendActionBar(Player player, String message) {

        CraftPlayer p = (CraftPlayer) player;


        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");

        PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc);

        p.getHandle().playerConnection.sendPacket(ppoc);

    }


    public static void setHeaderAndFooter(Player p, String header, String footer) {

        PacketContainer pc = ProtocolLibrary.getProtocolManager().createPacket(PacketType.Play.Server.PLAYER_LIST_HEADER_FOOTER);


        pc.getChatComponents().write(0, WrappedChatComponent.fromText(header)).write(1, WrappedChatComponent.fromText(footer));

        try {

            ProtocolLibrary.getProtocolManager().sendServerPacket(p, pc);

        } catch (Exception e) {

            e.printStackTrace();

        }

    }


    public static void sendTitle(Player p, String text, int stay, int fade) {

        PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + text + "\"}"), fade, stay, fade);

        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);

    }


    public static void sendSubTitle(Player p, String text, int stay, int fade) {

        PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + text + "\"}"), fade, stay, fade);

        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);

    }


    public static void sendTitleAndSubtitle(Player p, String main, String sub, int stay, int fade) {

        PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + main + "\"}"), fade, stay, fade);

        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);

        PacketPlayOutTitle subtitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, IChatBaseComponent.ChatSerializer.a("{\"text\":\"" + sub + "\"}"), fade, stay, fade);

        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(subtitle);

    }

}


