/*    */
package me.PieckDEV.Core.Utils;
/*    */ 
/*    */

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*    */
/*    */
/*    */
/*    */
/*    */
/*    */

/*    */
/*    */ public class LightningUtil
/*    */ {
    /*    */
    public static void sendLightning(Player p, Location l)
/*    */ {
/* 15 */
        Class<?> light = getNMSClass("EntityLightning");
/*    */
        try
/*    */ {
/* 18 */
            Constructor<?> constu =
/* 19 */         light
/* 20 */.getConstructor(getNMSClass("World"),
/* 21 */         Double.TYPE, Double.TYPE,
/* 22 */         Double.TYPE, Boolean.TYPE, Boolean.TYPE);
/* 23 */
            Object wh = p.getWorld().getClass().getMethod("getHandle").invoke(p.getWorld());
/* 24 */
            Object lighobj = constu.newInstance(wh, Double.valueOf(l.getX()), Double.valueOf(l.getY()), Double.valueOf(l.getZ()), Boolean.valueOf(false), Boolean.valueOf(false));
/*    */       
/* 26 */
            Object obj =
/* 27 */         getNMSClass("PacketPlayOutSpawnEntityWeather")
/* 28 */.getConstructor(new Class[]{getNMSClass("Entity")}).newInstance(lighobj);
/*    */       
/* 30 */
            sendPacket(p, obj);
/* 31 */
            p.playSound(p.getLocation(), Sound.ENTITY_LIGHTNING_THUNDER, 100.0F, 1.0F);
/*    */
        }
/*    */ catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException e)
/*    */ {
/* 35 */
            e.printStackTrace();
/*    */
        }
/*    */
    }

    /*    */
/*    */
    public static Class<?> getNMSClass(String name)
/*    */ {
/* 41 */
        String version = org.bukkit.Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
/*    */
        try
/*    */ {
/* 44 */
            return Class.forName("net.minecraft.server." + version + "." + name);
/*    */
        }
/*    */ catch (ClassNotFoundException e)
/*    */ {
/* 48 */
            e.printStackTrace();
/*    */
        }
/* 50 */
        return null;
/*    */
    }

    /*    */
/*    */
    public static void sendPacket(Player player, Object packet)
/*    */ {
/*    */
        try
/*    */ {
/* 57 */
            Object handle = player.getClass().getMethod("getHandle").invoke(player);
/* 58 */
            Object playerConnection = handle.getClass().getField("playerConnection").get(handle);
/* 59 */
            playerConnection.getClass().getMethod("sendPacket", getNMSClass("Packet"))
/* 60 */.invoke(playerConnection, packet);
/*    */
        }
/*    */ catch (Exception e)
/*    */ {
/* 64 */
            e.printStackTrace();
/*    */
        }
/*    */
    }
/*    */
}


/* Location:              C:\Users\Zorgklas 2\Desktop\PardoesCraft\plugins\PardoesCraft.jar!\me\CaptainXan\PardoesCraft\Utils\LightningUtil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */