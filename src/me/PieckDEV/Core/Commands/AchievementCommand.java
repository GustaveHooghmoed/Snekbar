package me.PieckDEV.Core.Commands;


import me.CaptainXan.PardoesCraft.Utils.MessageUtil;
import me.CaptainXan.PardoesCraft.Utils.Objects.Achievement;
import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Managers.AchievementManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;


public class AchievementCommand
        implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String name, String[] args) {

        if (cmd.getName().equalsIgnoreCase("achievement")) {

            if (sender.isOp()) {

                if (args.length == 3) {

                    if ((args[0].equalsIgnoreCase("give")) || (args[0].equalsIgnoreCase("remove"))) {

                        if (AchievementManager.getAchievementFromName(args[1].toLowerCase()) != null) {

                            Achievement ach = AchievementManager.getAchievementFromName(args[1].toLowerCase());

                            if (Bukkit.getPlayer(args[2]) != null) {

                                if (args[0].equalsIgnoreCase("give")) {

                                    AchievementManager.giveAchievement(Bukkit.getPlayer(args[2]), ach);

                                }

                                if (args[0].equalsIgnoreCase("remove")) {

                                    AchievementManager.removeAchievement(Bukkit.getPlayer(args[2]), ach);

                                }

                            } else {

                                sender.sendMessage(MS.c("&cEr is geen speler gevonden met de naam '" + args[2] + "'!"));

                            }

                        } else {

                            sender.sendMessage(MS.c("&cEr is geen achievement gevonden met de naam '" + args[1].toLowerCase() + "&c'!"));

                        }

                    } else {

                        sender.sendMessage(MS.c("&4Gebruik: &c/achievement <give|remove> <achievement> <speler>"));

                    }

                } else {

                    sender.sendMessage(MS.c("&4Gebruik: &c/achievement <give|remove> <achievement> <speler>"));

                }

            } else {

                sender.sendMessage(MessageUtil.noPermissionForCmd());

            }

        }

        return false;

    }

}


