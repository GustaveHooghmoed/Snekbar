package me.PieckDEV.Core.Listeners.Listeners;


import me.CaptainXan.PardoesCraft.Utils.Guests.Guest;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Managers.CostumeManager;
import me.PieckDEV.Core.Managers.RideManager;
import me.PieckDEV.Core.Menu.AchievementMenu;
import me.PieckDEV.Core.Menu.CostumeMenu;
import me.PieckDEV.Core.Menu.RideMenu;
import me.PieckDEV.Core.Rides.Enums.RideStatus;
import me.PieckDEV.Core.Rides.Ride;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class InventoryClickListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if ((e.getCurrentItem() != null) &&
                (e.getCurrentItem().hasItemMeta()) &&
                (e.getCurrentItem().getType() == Material.BARRIER) &&
                (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(c("&3Sluit menu")))) {

            p.closeInventory();

        }


        if (e.getSlotType() == InventoryType.SlotType.ARMOR) {

            e.setCancelled(true);

            p.closeInventory();

            p.sendMessage(c("&3Gebruik het kostuum menu als je van kleding wilt veranderen!"));

        }

        if (e.getInventory().getName().contains(c("&8&nAchievements"))) {

            e.setCancelled(true);

        }

        if (e.getInventory().getName().contains(c("&8&nRegels"))) {

            e.setCancelled(true);

        }


        if (e.getInventory().getName().contains(c("&8&nKostuums"))) {

            e.setCancelled(true);

            if (e.getCurrentItem() != null) {

                if ((e.getCurrentItem().getType() == Material.BARRIER) &&
                        (e.getCurrentItem().hasItemMeta())) {

                    ItemMeta meta = e.getCurrentItem().getItemMeta();

                    if (meta.getDisplayName().equalsIgnoreCase(c("&3Verwijder kostuum"))) {

                        if (CostumeManager.equippedCostume(p) != null) {

                            CostumeManager.unequipCostume(p);

                            p.closeInventory();

                        } else {

                            p.sendMessage(c("&3Je draagt momenteel geen kostuum!"));

                        }

                    }

                }


                if ((e.getCurrentItem().getType() == Material.ARMOR_STAND) &&
                        (e.getCurrentItem().hasItemMeta())) {

                    ItemMeta meta = e.getCurrentItem().getItemMeta();

                    if (meta.getDisplayName().contains(c("&6Kostuum:"))) {

                        Costume c = CostumeManager.getCostumeFromTitle(ChatColor.stripColor(meta.getDisplayName()).replace("Kostuum: ", ""));

                        if (c != null) {

                            CostumeManager.equipCostume(p, c);

                            p.closeInventory();

                        }

                    }

                }

            }

        }


        if (e.getInventory().getName().contains(c("&8&nKostuum kopen"))) {

            e.setCancelled(true);

            if ((e.getCurrentItem() != null) &&
                    (e.getCurrentItem().getType() == Material.STAINED_CLAY) &&
                    (e.getCurrentItem().hasItemMeta())) {

                ItemMeta meta = e.getCurrentItem().getItemMeta();

                if (meta.getDisplayName().equalsIgnoreCase(c("&6Sluit menu"))) {

                    p.closeInventory();

                }

                if (meta.getDisplayName().equalsIgnoreCase(c("&6Koop kostuum"))) {

                    Costume c = CostumeMenu.costume.get(p);

                    if (me.CaptainXan.PardoesCraft.Utils.EconomyUtil.hasMoney(p, c.getPrice())) {

                        if (!CostumeManager.hasCostume(p, c)) {

                            Guest g = new Guest(p);

                            g.takeMoney(c.getPrice());

                            p.closeInventory();

                            CostumeManager.addCostume(p, c);

                        } else {

                            p.closeInventory();

                            p.sendMessage(c("&3Je hebt dit kostuum al in je bezit!"));

                        }

                    } else {

                        p.closeInventory();

                        p.sendMessage(c("&3Je hebt niet genoeg dukaten om dit kostuum te kopen!"));

                    }

                }

            }

        }


        if (e.getInventory().getName().equalsIgnoreCase(c("           &8&nPieckCraft"))) {

            e.setCancelled(true);

            if (e.getCurrentItem() != null) {

                ItemStack is = e.getCurrentItem();

                if (is.hasItemMeta()) {

                    ItemMeta im = is.getItemMeta();

                    if (im.getDisplayName().equalsIgnoreCase(c("&3Attracties"))) {

                        p.openInventory(RideMenu.classicRideMenu());

                    }

                    if (im.getDisplayName().equalsIgnoreCase(c("&3Achievements"))) {

                        p.openInventory(AchievementMenu.achievementmenu(p));

                    }

                    if (im.getDisplayName().equalsIgnoreCase(c("&3Kostuums"))) {

                        p.openInventory(CostumeMenu.ownedcostumemenu(p));

                    }


                    if (im.getDisplayName().equalsIgnoreCase(c("&3Regels"))) {

                        p.openInventory(me.CaptainXan.PardoesCraft.Menu.RegelMenu.RegelMenu(p));

                    }

                }

            }


        }


        if (e.getInventory().getName().contains(c("           &8&nAttracties"))) {

            e.setCancelled(true);

            if (e.getCurrentItem() != null) {

                ItemStack is = e.getCurrentItem();

                if ((is.getType() == Material.PAPER) &&
                        (is.hasItemMeta())) {

                    ItemMeta im = is.getItemMeta();

                    if (im.getDisplayName().equalsIgnoreCase(c("&6Alle attracties"))) {

                        p.openInventory(RideMenu.classicRideMenu());

                    }

                    if (im.getDisplayName().equalsIgnoreCase(c("&6Attracties per rijk"))) {

                        p.openInventory(RideMenu.realmRideMenu());

                    }

                }


                if ((is.getType() == Material.STAINED_CLAY) &&
                        (is.hasItemMeta())) {

                    ItemMeta im = is.getItemMeta();

                    if (im.getDisplayName().contains(c("&6Attractie: "))) {

                        Ride ride = RideManager.getRideFromTitle(ChatColor.stripColor(im.getDisplayName()).replace("Attractie: ", ""));

                        if (ride.getStatus() == RideStatus.OPEN) {

                            p.closeInventory();

                            p.performCommand("warp " + ride.getName());

                        }

                        if (ride.getStatus() == RideStatus.CLOSED) {

                            p.sendMessage(c("&3Je kan niet naar een gesloten attractie warpen!"));

                        }

                        if (ride.getStatus() == RideStatus.MAINTENANCE) {

                            p.sendMessage(c("&3Je kan niet naar deze attractie warpen omdat hij in onderhoud is!"));

                        }

                    }

                }

            }

        }

    }


    private String c(String string) {

        return MS.c(string);

    }

}


