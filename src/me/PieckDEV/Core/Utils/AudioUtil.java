package me.PieckDEV.Core.Utils;


import com.google.gson.JsonObject;
import me.CaptainXan.AudioserverV2.API.AudioAPI;
import me.CaptainXan.AudioserverV2.Enums.PacketType;
import me.CaptainXan.AudioserverV2.Objects.AudioPlayer;
import org.bukkit.entity.Player;
import org.java_websocket.WebSocket;

import java.util.HashMap;


public class AudioUtil {
    public static HashMap<String, Player> tempkeys = new HashMap();


    public static void disconnect(WebSocket ws, AudioPlayer p) {

        ws.close();

    }


    public static void forceSwitch(WebSocket socket, AudioPlayer a) {

        socket.close();

    }

    public static void play(Player p, String url) {
        if (AudioAPI.getPlayerManager().isAudioPlayer(p)){
            AudioPlayer ap = AudioAPI.getPlayerManager().getAudioPlayer(p);
            WebSocket socket = ap.getSocket();
            {
                JsonObject message = new JsonObject();
                message.addProperty("id", PacketType.PLAY_SOUND.getId());
                message.addProperty("sound", url);
                AudioAPI.getSocketManager().sendData(socket, message.toString());
            }
        }
    }
}


