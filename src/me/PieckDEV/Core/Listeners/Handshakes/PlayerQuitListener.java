package me.PieckDEV.Core.Listeners.Handshakes;


import me.IkeaKasten.Utils.MS;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;


public class PlayerQuitListener
        implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {

        Player p = e.getPlayer();


        e.setQuitMessage(MS.c("&8&oTot ziens " + p.getName() + "!"));

    }

}


