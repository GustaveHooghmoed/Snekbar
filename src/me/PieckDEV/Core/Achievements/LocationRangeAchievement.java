package me.PieckDEV.Core.Achievements;

import me.PieckDEV.Core.Utils.EntityUtil;
import me.PieckDEV.Core.Utils.Objects.Achievement;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class LocationRangeAchievement {

    private final Location location;
    private final Achievement achievement;
    private final double range;

    public LocationRangeAchievement(Location location, Achievement achievement, double range) {
        this.location = location;
        this.achievement = achievement;
        this.range = range;
    }

    public Location getLocation() {
        return location;
    }

    public Achievement getAchievement() {
        return achievement;
    }

    public double getRange() {
        return range;
    }

    public ArrayList<Player> getPlayersInRange() {
        ArrayList<Player> nearby = new ArrayList<Player>();
        double range = this.range;
        for (Entity e : EntityUtil.getNearbyEntities(location, range)) {
            if (e instanceof Player) {
                nearby.add((Player) e);
            }
        }
        return nearby;
    }

    // Lars is cool
}
