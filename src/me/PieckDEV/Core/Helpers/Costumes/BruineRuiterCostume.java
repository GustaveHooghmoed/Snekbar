package me.PieckDEV.Core.Helpers.Costumes;


import me.CaptainXan.PardoesCraft.Utils.Enums.CostumeType;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import me.CaptainXan.PardoesCraft.Utils.ItemStackUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class BruineRuiterCostume
        implements Costume {

    public String getName() {

        return "bruineruiter";

    }


    public String getTitle() {

        return "Bruine ruiter";

    }


    public ItemStack getHelmet() {

        ItemStack is = new ItemStack(Material.LEATHER_HELMET);

        ItemStackUtil.setLeatherRGB(is, 51, 25, 0);

        return is;

    }


    public ItemStack getChestplate() {

        ItemStack is = new ItemStack(Material.LEATHER_CHESTPLATE);

        ItemStackUtil.setLeatherRGB(is, 51, 25, 0);

        return is;

    }


    public ItemStack getLeggings() {

        ItemStack is = new ItemStack(Material.LEATHER_LEGGINGS);

        return is;

    }


    public ItemStack getBoots() {

        ItemStack is = new ItemStack(Material.LEATHER_BOOTS);

        ItemStackUtil.setLeatherRGB(is, 51, 25, 0);

        return is;

    }


    public CostumeType getType() {

        return CostumeType.GENERAL;

    }


    public int getPrice() {

        return 2500;

    }

}


