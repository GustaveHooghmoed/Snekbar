package me.PieckDEV.Core.Utils.Objects;


import me.CaptainXan.PardoesCraft.Main.Main;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;


public class MoveableJet {
    public Location location;
    public float pitch = -90.0F;
    public float yaw = 0.0F;
    public float range = 1.2F;
    public boolean isOn = false;


    public MoveableJet(Location location) {

        this.location = location;

        this.location.setPitch(this.pitch);

        this.location.setYaw(this.yaw);

    }


    public void execute() {

        toggle();

    }


    public float getYaw() {

        return this.yaw;

    }


    public void setYaw(float yaw) {

        this.yaw = yaw;

        this.location.setYaw(yaw);

    }


    public float getPitch() {

        return this.pitch;

    }


    public void setPitch(float pitch) {

        this.pitch = pitch;

        this.location.setPitch(pitch);

    }


    public float getRange() {

        return this.range;

    }


    public void setRange(float range) {

        this.range = range;

    }


    public void reset() {

        this.pitch = -90.0F;

        this.yaw = 0.0F;

        this.range = 1.2F;

        this.location.setPitch(-90.0F);

        this.location.setYaw(0.0F);

    }


    public void toggle() {

        if (!this.isOn) {

            this.isOn = true;


            new BukkitRunnable() {

                public void run() {

                    if (MoveableJet.this.isOn) {

                        Vector v = MoveableJet.this.location.getDirection().multiply(MoveableJet.this.range);

                        CustomFallingBlock fb = new CustomFallingBlock(MoveableJet.this.location, 19, (byte) 0);

                        fb.setVelocity(v);

                        fb.setRemoveTime((long) (v.getY() * MoveableJet.this.range * 32.0D));

                        fb.setAutomaticRemove(true);

                    } else {

                        cancel();

                    }

                }

            }.runTaskTimer(Main.pl, 0L, 1L);

        } else {

            this.isOn = false;

        }

    }

}


