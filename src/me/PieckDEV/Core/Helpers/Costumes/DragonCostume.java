package me.PieckDEV.Core.Helpers.Costumes;


import me.CaptainXan.PardoesCraft.Utils.Enums.CostumeType;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import me.CaptainXan.PardoesCraft.Utils.ItemStackUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class DragonCostume
        implements Costume {

    public String getName() {

        return "dragon";

    }


    public String getTitle() {

        return "Ender dragon";

    }


    public ItemStack getHelmet() {

        return new ItemStack(Material.SKULL_ITEM, 1, (short) 5);

    }


    public ItemStack getChestplate() {

        ItemStack is = new ItemStack(Material.LEATHER_CHESTPLATE);

        ItemStackUtil.setLeatherRGB(is, 32, 32, 32);

        return is;

    }


    public ItemStack getLeggings() {

        ItemStack is = new ItemStack(Material.LEATHER_LEGGINGS);

        ItemStackUtil.setLeatherRGB(is, 32, 32, 32);

        return is;

    }


    public ItemStack getBoots() {

        ItemStack is = new ItemStack(Material.LEATHER_BOOTS);

        ItemStackUtil.setLeatherRGB(is, 32, 32, 32);

        return is;

    }


    public CostumeType getType() {

        return CostumeType.VIP;

    }


    public int getPrice() {

        return 10000;

    }

}


