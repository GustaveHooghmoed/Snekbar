package me.PieckDEV.Core.Utils;


import me.IkeaKasten.Utils.MS;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.ArrayList;


public class ItemStackUtil {

    private static String c(String string) {

        return MS.c(string);

    }


    private static void cc(String[] ruleslore, ItemStack rides) {

        ItemMeta im = rides.getItemMeta();

        im.setLore(MS.cc(ruleslore));

        rides.setItemMeta(im);

    }


    public static void setLeatherRGB(ItemStack is, int r, int g, int b) {

        LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();


        meta.setColor(Color.fromRGB(r, g, b));


        is.setItemMeta(meta);

    }


    public static Item dropItem(ItemStack is, Location loc, boolean pickup) {

        Item i = loc.getWorld().dropItem(loc, is);

        if (!pickup) {

            i.setPickupDelay(Integer.MAX_VALUE);

        }

        return i;

    }


    public static void setName(ItemStack is, String name) {

        ItemMeta im = is.getItemMeta();

        im.setDisplayName(c(name));


        is.setItemMeta(im);

    }


    public static void setLore(ItemStack is, String[] lore) {

        ArrayList<String> l = new ArrayList();

        String[] arrayOfString;

        int j = (arrayOfString = lore).length;

        for (int i = 0; i < j; i++) {

            String s = arrayOfString[i];

            l.add(s);

        }

        cc(lore, is);

    }

}


