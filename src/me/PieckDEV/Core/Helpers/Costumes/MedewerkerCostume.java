package me.PieckDEV.Core.Helpers.Costumes;


import me.CaptainXan.PardoesCraft.Utils.Enums.CostumeType;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import me.CaptainXan.PardoesCraft.Utils.ItemStackUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class MedewerkerCostume
        implements Costume {

    public String getName() {

        return "Medewerker";

    }


    public String getTitle() {

        return "Medewerker";

    }


    public ItemStack getHelmet() {

        ItemStack is = new ItemStack(Material.AIR);

        return is;

    }


    public ItemStack getChestplate() {

        ItemStack is = new ItemStack(Material.LEATHER_CHESTPLATE);

        ItemStackUtil.setLeatherRGB(is, 66, 182, 244);

        return is;

    }


    public ItemStack getLeggings() {

        ItemStack is = new ItemStack(Material.LEATHER_LEGGINGS);

        ItemStackUtil.setLeatherRGB(is, 107, 60, 34);

        return is;

    }


    public ItemStack getBoots() {

        ItemStack is = new ItemStack(Material.LEATHER_BOOTS);

        ItemStackUtil.setLeatherRGB(is, 34, 34, 34);

        return is;

    }


    public CostumeType getType() {

        return CostumeType.VIP;

    }


    public int getPrice() {

        return 10;

    }

}


