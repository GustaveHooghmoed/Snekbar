package me.PieckDEV.Core.Utils.Objects;


import net.minecraft.server.v1_12_R1.Vector3f;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;


public class Path3D {
    private ArrayList<Location> points = new ArrayList();
    private ArrayList<Location> locations = new ArrayList();
    private Spline3D spline = new Spline3D();


    public Path3D() {

        Bukkit.getLogger().info("Created a new 3D path!");

    }


    public ArrayList<Location> getPoints() {

        return this.points;

    }


    public void addPoint(Location point) {

        if (!this.points.contains(point)) {

            this.points.add(point);

        }

    }


    public void removePoint(Location point) {

        if (this.points.contains(point)) {

            this.points.remove(point);

        }

    }


    public ArrayList<Location> getLocations() {

        return this.locations;

    }


    public void calculate() {

        this.spline.getPoints().clear();

        this.locations.clear();

        for (Location point : this.points) {

            this.spline.addPoint(new Vector3f((float) point.getX(), (float) point.getY(), (float) point.getZ()));

        }

        this.spline.calcSpline();

        for (double i = 0.0D; i < this.points.size() * 500; i += 0.2D) {

            Vector3f points = this.spline.getPoint((float) i / (this.points.size() * 500));

            this.locations.add(new Location(this.points.get(0).getWorld(), points.getX(), points.getY(), points.getZ()));

        }

    }

}


