package me.PieckDEV.Core.Schedulers;


import me.CaptainXan.PardoesCraft.Main.Main;
import me.CaptainXan.PardoesCraft.Utils.Guests.Guest;
import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Utils.EconomyUtil;
import me.PieckDEV.Core.Utils.TitleUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;


public class MoneyScheduler extends BukkitRunnable {

    public void run() {

        for (Player p : Bukkit.getOnlinePlayers()) {

            int amount = EconomyUtil.getScheduledMoney(p);

            if (!Main.ess.getUser(p).isAfk()) {

                Guest guest = new Guest(p);

                guest.giveMoney(amount);

                TitleUtil.sendActionBar(p, MS.c("&7Je hebt &c" + amount + " &7dukaten gekregen omdat je online bent!"));

            } else {

                p.sendMessage(MS.c("&7Je hebt geen dukaten gekregen omdat je niet aanwezig bent!"));

            }

        }

    }

}


