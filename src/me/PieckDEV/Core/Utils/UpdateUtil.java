package me.PieckDEV.Core.Utils;


import me.CaptainXan.PardoesCraft.Main.Main;
import me.CaptainXan.PardoesCraft.Utils.Enums.UpdateType;
import me.PieckDEV.Core.Events.UpdateEvent;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;


public class UpdateUtil {

    public static void setup() {

        new BukkitRunnable() {

            public void run() {

                Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateType.TICK));

            }

        }.runTaskTimer(Main.pl, 1L, 1L);


        new BukkitRunnable() {

            public void run() {

                Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateType.SECOND));

            }

        }.runTaskTimer(Main.pl, 1L, 20L);


        new BukkitRunnable() {

            public void run() {

                Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateType.MINUTE));

            }

        }.runTaskTimer(Main.pl, 1L, 1200L);


        new BukkitRunnable() {

            public void run() {

                Bukkit.getPluginManager().callEvent(new UpdateEvent(UpdateType.HOUR));

            }

        }.runTaskTimer(Main.pl, 1L, 72000L);

    }

}


