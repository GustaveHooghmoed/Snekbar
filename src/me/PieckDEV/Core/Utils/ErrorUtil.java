package me.PieckDEV.Core.Utils;


import me.IkeaKasten.Utils.Broadcast;


public class ErrorUtil {

    public static void reportException(Exception e, Class<?> c) {

        Broadcast.withPerm("&4Unhandled exception caught: &c" + e.getMessage() + " in " + c.getName(), "PC.Admin");

        e.printStackTrace();

    }

}


