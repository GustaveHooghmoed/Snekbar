package me.PieckDEV.Core.Utils.RawMessage;


import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.Arrays;


public class RawMessages
        implements Listener {

    public static void sendRawMessage(Player p, String s, String s1, String s2, String s3) {

        JSONChatMessage message = new JSONChatMessage(s, null, null);

        JSONChatExtra extra = new JSONChatExtra(s1, JSONChatColor.WHITE, Arrays.asList(JSONChatFormat.NORMAL));

        extra.setHoverEvent(JSONChatHoverEventType.SHOW_TEXT, s2);

        extra.setClickEvent(JSONChatClickEventType.RUN_COMMAND, s3);

        message.addExtra(extra);

        message.sendToPlayer(p);

    }


    public static void sendRawMessageURL(Player p, String s, String s1, String s2, String s3) {

        JSONChatMessage message = new JSONChatMessage(s, null, null);

        JSONChatExtra extra = new JSONChatExtra(s1, JSONChatColor.WHITE, Arrays.asList(JSONChatFormat.NORMAL));

        extra.setHoverEvent(JSONChatHoverEventType.SHOW_TEXT, s2);

        extra.setClickEvent(JSONChatClickEventType.OPEN_URL, s3);

        message.addExtra(extra);

        message.sendToPlayer(p);

    }

}


