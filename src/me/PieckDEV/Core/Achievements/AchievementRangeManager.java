package me.PieckDEV.Core.Achievements;

import me.PieckDEV.Core.Events.UpdateEvent;
import me.PieckDEV.Core.Managers.AchievementManager;
import me.PieckDEV.Core.Utils.Enums.UpdateType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;

public class AchievementRangeManager implements Listener {

    public static ArrayList<LocationRangeAchievement> arrayList = new ArrayList<>();

    public static void registerLocationRangeAchievement(LocationRangeAchievement locach) {
        arrayList.add(locach);
    }

    @EventHandler
    public void onUpdate(UpdateEvent e) {
        if (e.getType() == UpdateType.SECOND) {
            for (LocationRangeAchievement locationRangeAchievement : arrayList) {
                if (locationRangeAchievement.getPlayersInRange() != null) {
                    ArrayList<Player> players = locationRangeAchievement.getPlayersInRange();
                    for (Player player : players) {
                        if (!AchievementManager.hasAchievement(player, locationRangeAchievement.getAchievement())) {
                            AchievementManager.giveAchievement(player, locationRangeAchievement.getAchievement());
                        }
                    }
                }
            }
        }
    }
}
