package me.PieckDEV.Core.Listeners.Handshakes;


import me.CaptainXan.PardoesCraft.Utils.Guests.Guest;
import me.CaptainXan.PardoesCraft.Utils.NameUtil;
import me.CaptainXan.PardoesCraft.Utils.TitleUtil;
import me.IkeaKasten.Utils.MS;
import net.ess3.api.events.AfkStatusChangeEvent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.Random;


public class AfkStatusChangeListener
        implements Listener {

    @EventHandler
    public void onChange(AfkStatusChangeEvent e) {

        Player p = e.getAffected().getBase();

        if (!e.getValue()) {

            TitleUtil.sendTitleAndSubtitle(p, MS.c("&3Welkom terug!"), "", 20, 1);


            Guest g = new Guest(p);

            String prefix = ChatColor.translateAlternateColorCodes('&', g.getGroup().getOwnPrefix());

            NameUtil.setTabName(p, prefix.substring(0, 2) + p.getName());

        } else {

            Guest g = new Guest(p);

            String prefix = ChatColor.translateAlternateColorCodes('&', g.getGroup().getOwnPrefix());

            NameUtil.setTabName(p, MS.c("&7[AFK] " + prefix.substring(0, 2) + p.getName()));


            String een = MS.c("&6Geniet van je uitzicht!");

            String twee = MS.c("&6heeft Klaas vaak je laten slapen?");

            String drie = MS.c("&6Waarom ben je weg?");

            String vier = MS.c("&6Kom je weer terug?");

            String vijf = MS.c("&6Tot zo!");

            ArrayList<String> ma = new ArrayList();

            ma.add(een);

            ma.add(twee);

            ma.add(drie);

            ma.add(vier);

            ma.add(vijf);


            Random r = new Random();


            TitleUtil.sendTitleAndSubtitle(p, ma.get(r.nextInt(5)).toString(), "", 50, 1);

        }

    }

}


