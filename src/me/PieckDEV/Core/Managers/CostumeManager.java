package me.PieckDEV.Core.Managers;


import me.CaptainXan.PardoesCraft.Helpers.Costumes.*;
import me.CaptainXan.PardoesCraft.Utils.ErrorUtil;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import me.CaptainXan.PardoesCraft.Utils.ItemStackUtil;
import me.IkeaKasten.Utils.MS;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.util.ArrayList;


public class CostumeManager {
    public static ArrayList<Costume> costumes = new ArrayList();


    public static void setup() {

        costumes.add(new PardoesCostume());

        costumes.add(new PardijnCostume());

        costumes.add(new RodeRuiterCostume());

        costumes.add(new BlauweRuiterCostume());

        costumes.add(new GroeneRuiterCostume());

        costumes.add(new BruineRuiterCostume());

        costumes.add(new PaarseRuiterCostume());

        costumes.add(new DeveloperCostume());

        costumes.add(new DragonCostume());

        costumes.add(new KompelCostume());

        costumes.add(new MedewerkerCostume());

    }


    public static void equipCostume(Player p, Costume c) {

        ItemStack helmet = c.getHelmet();

        ItemStackUtil.setName(helmet, MS.c("&6" + c.getTitle() + " hoed"));

        ItemStack chestplate = c.getChestplate();

        ItemStackUtil.setName(chestplate, MS.c("&6" + c.getTitle() + " shirt"));

        ItemStack leggings = c.getLeggings();

        ItemStackUtil.setName(leggings, MS.c("&6" + c.getTitle() + " broek"));

        ItemStack boots = c.getBoots();

        ItemStackUtil.setName(boots, MS.c("&6" + c.getTitle() + " schoenen"));


        p.getEquipment().setHelmet(helmet);

        p.getEquipment().setChestplate(chestplate);

        p.getEquipment().setLeggings(leggings);

        p.getEquipment().setBoots(boots);

        p.updateInventory();


        p.playSound(p.getLocation(), Sound.ITEM_ARMOR_EQUIP_LEATHER, 10.0F, 1.0F);


        File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/costumes.yml");

        if (f.exists()) {

            YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

            conf.set("equipped", c.getName());

            try {

                conf.save(f);

            } catch (Exception e) {

                ErrorUtil.reportException(e, AchievementManager.class);

            }

        }

    }


    public static void unequipCostume(Player p) {

        p.getEquipment().setHelmet(new ItemStack(Material.AIR));

        p.getEquipment().setChestplate(new ItemStack(Material.AIR));

        p.getEquipment().setLeggings(new ItemStack(Material.AIR));

        p.getEquipment().setBoots(new ItemStack(Material.AIR));

        p.updateInventory();


        File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/costumes.yml");

        if (f.exists()) {

            YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

            conf.set("equipped", "none");

            try {

                conf.save(f);

            } catch (Exception e) {

                ErrorUtil.reportException(e, AchievementManager.class);

            }

        }

    }


    public static Costume getCostumeFromName(String name) {

        for (Costume c : costumes) {

            if (c.getName().equalsIgnoreCase(name)) {

                return c;

            }

        }

        return null;

    }


    public static Costume getCostumeFromTitle(String title) {

        for (Costume c : costumes) {

            if (c.getTitle().equalsIgnoreCase(title)) {

                return c;

            }

        }

        return null;

    }


    public static Costume equippedCostume(Player p) {

        File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/costumes.yml");

        if (f.exists()) {

            YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

            String eqq = conf.getString("equipped");

            if (!eqq.equalsIgnoreCase("none")) {

                Costume c = getCostumeFromName(eqq);

                return c;

            }

        }

        return null;

    }


    public static void addCostume(Player p, Costume c) {

        if (!hasCostume(p, c)) {

            File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/costumes.yml");

            YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

            ArrayList<String> list = (ArrayList) conf.getStringList("owns");


            list.add(c.getName());

            conf.set("equipped", "none");
            conf.set("owns", list);

            try {

                conf.save(f);

            } catch (Exception e) {

                ErrorUtil.reportException(e, CostumeManager.class);

            }

        }

    }


    public static ArrayList<Costume> getCostumes(Player p) {

        File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/costumes.yml");

        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

        ArrayList<String> list = (ArrayList) conf.getStringList("owns");

        ArrayList<Costume> clist = new ArrayList();

        for (String s : list) {

            clist.add(getCostumeFromName(s));

        }

        return clist;

    }


    public static boolean hasCostume(Player p, Costume c) {

        File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/costumes.yml");

        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

        ArrayList<String> list = (ArrayList) conf.getStringList("owns");


        return list.contains(c.getName().toLowerCase());

    }


    public static void setFile(Player p) {

        File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/costumes.yml");

        if (!f.exists()) {

            YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

            conf.set("equipped", "none");

            conf.set("owns", new ArrayList());

            try {

                conf.save(f);

            } catch (Exception e) {

                ErrorUtil.reportException(e, AchievementManager.class);

            }

        }

    }

}


