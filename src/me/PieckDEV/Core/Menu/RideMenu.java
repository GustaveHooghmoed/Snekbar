package me.PieckDEV.Core.Menu;


import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Managers.RideManager;
import me.PieckDEV.Core.Rides.Enums.Realm;
import me.PieckDEV.Core.Rides.Enums.RideStatus;
import me.PieckDEV.Core.Rides.Ride;
import me.PieckDEV.Core.Utils.ItemStackUtil;
import me.PieckDEV.Core.Utils.MenuUtil;
import me.PieckDEV.Core.Utils.StringUtil;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


public class RideMenu {

    public static Inventory classicRideMenu() {

        Inventory inv = MenuUtil.menu(MS.c("              &8&nAttracties"), 4);


        ItemStack classic = new ItemStack(Material.PAPER);

        ItemStackUtil.setName(classic, "&6Alle attracties");


        ItemStack realms = new ItemStack(Material.PAPER);

        ItemStackUtil.setName(realms, "&6Attracties per rijk");


        inv.setItem(0, classic);

        inv.setItem(1, realms);

        for (int i = 9; i - 9 < RideManager.rides.size(); i++) {

            inv.setItem(i, rideItem(RideManager.sortByStatus(RideManager.rides).get(i - 9)));

        }

        return inv;

    }


    public static Inventory realmRideMenu() {

        Inventory inv = MenuUtil.menu(MS.c("              &8&nAttracties"), 6);


        ItemStack classic = new ItemStack(Material.PAPER);

        ItemStackUtil.setName(classic, "&6Alle attracties");


        ItemStack realms = new ItemStack(Material.PAPER);

        ItemStackUtil.setName(realms, "&6Attracties per rijk");


        ItemStack anderrijk = new ItemStack(Material.NETHER_STAR);

        ItemStackUtil.setName(anderrijk, "&3Anderrijk");


        ItemStack reizenrijk = new ItemStack(Material.NETHER_STAR);

        ItemStackUtil.setName(reizenrijk, "&6Reizenrijk");


        ItemStack ruigrijk = new ItemStack(Material.NETHER_STAR);

        ItemStackUtil.setName(ruigrijk, "&4Ruigrijk");


        ItemStack marerijk = new ItemStack(Material.NETHER_STAR);

        ItemStackUtil.setName(marerijk, "&2Marerijk");

        ItemStack fantasierijk = new ItemStack(Material.NETHER_STAR);

        ItemStackUtil.setName(fantasierijk, "&5Fantasierijk");

        inv.setItem(0, classic);

        inv.setItem(1, realms);

        inv.setItem(9, anderrijk);

        inv.setItem(18, reizenrijk);

        inv.setItem(27, ruigrijk);

        inv.setItem(36, marerijk);

        inv.setItem(45, fantasierijk);


        int an = 10;

        int rei = 19;

        int rui = 28;

        int ma = 37;

        int fa = 46;

        for (Ride ride : RideManager.sortRidesByRealm(Realm.ANDERRIJK)) {

            inv.setItem(an, rideItem(ride));

            an++;

        }

        for (Ride ride : RideManager.sortRidesByRealm(Realm.REIZENRIJK)) {

            inv.setItem(rei, rideItem(ride));

            rei++;

        }

        for (Ride ride : RideManager.sortRidesByRealm(Realm.RUIGRIJK)) {

            inv.setItem(rui, rideItem(ride));

            rui++;

        }

        for (Ride ride : RideManager.sortRidesByRealm(Realm.MARERIJK)) {

            inv.setItem(ma, rideItem(ride));

            ma++;

        }

        for (Ride ride : RideManager.sortRidesByRealm(Realm.FANTASIERIJK)) {

            inv.setItem(fa, rideItem(ride));

            fa++;

        }

        return inv;

    }


    public static String getStatus(Ride r) {

        String status = "In onderhoud";

        if (r.getStatus() == RideStatus.OPEN) {

            status = "Geopend";

        }

        if (r.getStatus() == RideStatus.CLOSED) {

            status = "Gesloten";

        }

        return status;

    }


    public static String getRealm(Ride r) {

        return StringUtil.getFirstUpper(r.getRealm().toString());

    }


    public static ItemStack rideItem(Ride r) {

        ItemStack item = new ItemStack(Material.STAINED_CLAY, 1, (short) rideColorValue(r));

        String[] lore = {"", "&eStatus: &7" + getStatus(r), "&eRijk: &7" + getRealm(r), "", "&8Klik om te warpen"};


        ItemStackUtil.setName(item, "&6Attractie: &e" + r.getTitle());

        ItemStackUtil.setLore(item, lore);


        return item;

    }


    public static int rideColorValue(Ride r) {

        int i = 9;

        if (r.getStatus() == RideStatus.OPEN) {

            i = 5;

        }

        if (r.getStatus() == RideStatus.CLOSED) {

            i = 14;

        }

        return i;

    }

}


