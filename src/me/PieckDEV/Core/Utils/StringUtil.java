package me.PieckDEV.Core.Utils;


import org.bukkit.entity.Player;

import java.util.Base64;


public class StringUtil {

    public static int countWords(String input) {

        int wordCount = 0;

        boolean isBlankSet = false;

        input = input.trim();

        for (int j = 0; j < input.length(); j++) {

            if (input.charAt(j) == ' ') {

                isBlankSet = true;

            } else if (isBlankSet) {

                wordCount++;

                isBlankSet = false;

            }

        }

        return wordCount + 1;

    }


    public static String longTextFromArg(String[] args, int ar) {

        String text = "";

        for (int i = ar; i < args.length; i++) {

            text = text + args[i] + " ";

        }

        return text.replace("&", "&");

    }


    public static String argsFromTo(String[] args, int from, int to) {

        String text = "";

        for (int i = from; i < to; i++) {

            text = text + args[i] + " ";

        }

        return text.replace("&", "&");

    }


    public static String get3rdPersonName(String name) {

        if (name.endsWith("s")) {

            return name + "'";

        }

        return name + "'s";

    }


    public static void sendEmptyMessage(Player p, int amount) {

        for (int i = 0; i < amount; i++) {

            p.sendMessage("   ");

        }

    }


    public static String getFirstUpper(String s) {

        return s.substring(0, 1).toUpperCase() + s.substring(1, s.length()).toLowerCase();

    }


    public static String centerText(String text, int max) {

        if (text.length() > max) {

            return text.substring(0, max);

        }

        int pad = (max - text.length()) / 2;

        for (int i = 0; i < pad; i++) {

            text = " " + text;

        }

        return text;

    }


    public static String ordinal(int i) {

        String[] sufixes = {"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};

        switch (i % 100) {

            case 11:

            case 12:

            case 13:

                return i + "th";

        }

        return sufixes[(i % 10)];

    }


    public static String generateKey(String pn) {

        byte[] encodedBytes = Base64.getEncoder().encode(pn.getBytes());

        return new String(encodedBytes);

    }


    public static String decode(String dec) {

        byte[] decodedBytes = Base64.getDecoder().decode(dec);

        System.out.print(dec);

        return new String(decodedBytes);

    }

}


