package me.PieckDEV.Core.Rides;


import me.PieckDEV.Core.Rides.Enums.Realm;
import me.PieckDEV.Core.Rides.Enums.RideStatus;
import me.PieckDEV.Core.Rides.Enums.RideType;
import me.PieckDEV.Core.Rides.Events.RideStatusChangeEvent;
import org.bukkit.Bukkit;


public class Ride {
    private String name;
    private String title;
    private RideType type;
    private RideStatus status = RideStatus.CLOSED;
    private Realm realm;


    public Ride(String name, String title, RideType type, Realm realm) {

        this.name = name;

        this.title = title;

        this.type = type;

        this.realm = realm;

    }


    public String getName() {

        return this.name;

    }


    public void setName(String name) {

        this.name = name;

    }


    public String getTitle() {

        return this.title;

    }


    public void setTitle(String title) {

        this.title = title;

    }


    public RideType getType() {

        return this.type;

    }


    public void setType(RideType type) {

        this.type = type;

    }


    public RideStatus getStatus() {

        return this.status;

    }


    public void setStatus(RideStatus status) {

        if (this.status != status) {

            Bukkit.getPluginManager().callEvent(new RideStatusChangeEvent(this, status));

        }

        this.status = status;

    }


    public Realm getRealm() {

        return this.realm;

    }


    public void setRealm(Realm realm) {

        this.realm = realm;

    }

}


