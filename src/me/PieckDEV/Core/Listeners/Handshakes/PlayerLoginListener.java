package me.PieckDEV.Core.Listeners.Handshakes;

import me.PieckDEV.Core.Managers.ServerManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

public class PlayerLoginListener implements Listener {

    @EventHandler
    public void onLogin(PlayerLoginEvent event){
        if(event.getResult() == PlayerLoginEvent.Result.KICK_WHITELIST){
            event.setKickMessage(ServerManager.getWhitelistMessage().toString());
        }
    }
}
