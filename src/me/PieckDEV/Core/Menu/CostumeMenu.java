package me.PieckDEV.Core.Menu;


import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Managers.CostumeManager;
import me.PieckDEV.Core.Utils.ItemStackUtil;
import me.PieckDEV.Core.Utils.MenuUtil;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;


public class CostumeMenu {
    public static HashMap<Player, Costume> costume = new HashMap();


    public static String c(String string) {

        return MS.c(string);

    }


    private static void cc(String[] ruleslore, ItemStack rides) {

        ItemMeta im = rides.getItemMeta();

        im.setLore(MS.cc(ruleslore));

        rides.setItemMeta(im);

    }


    public static Inventory ownedcostumemenu(Player p) {

        Inventory inv = MenuUtil.menu(c("              &8&nKostuums"), (int) Math.ceil(CostumeManager.getCostumes(p).size() / 9.0D) + 1);


        ItemStack info = new ItemStack(Material.BOOK);

        ItemStackUtil.setName(info, c("&3Kostuum info:"));

        String[] infolore = {"&7Je bezit &3" + CostumeManager.getCostumes(p).size() + " &7kostuum(s)!"};

        cc(infolore, info);

        inv.setItem(0, info);


        ItemStack unequip = new ItemStack(Material.BARRIER);

        ItemStackUtil.setName(unequip, c("&3Verwijder kostuum"));

        String[] unequiplore = {"&7Klik hier om je huidige kostuum te verwijderen!"};

        cc(unequiplore, unequip);

        inv.setItem(1, unequip);

        for (int i = 0; i < CostumeManager.getCostumes(p).size(); i++) {

            inv.setItem(i + 9, costumeItem(CostumeManager.getCostumes(p).get(i)));

        }

        return inv;

    }


    public static ItemStack costumeItem(Costume c) {

        ItemStack is = new ItemStack(Material.ARMOR_STAND);

        ItemStackUtil.setName(is, c("&6Kostuum: &e" + c.getTitle()));

        String[] lore = {"&7Klik hier om dit kostuum te dragen!"};

        cc(lore, is);


        return is;

    }


    public static Inventory shopcostumemenu(Player p, Costume c) {

        Inventory inv = MenuUtil.menu(c("&8Kostuum kopen"), 5);


        costume.put(p, c);


        ItemStack buy = new ItemStack(Material.STAINED_CLAY, 1, (short) 5);

        ItemStackUtil.setName(buy, "&6Koop kostuum");

        String[] lore = {"&7Klik hier om dit kostuum te kopen voor &e" + c.getPrice() + " &7dukaten!"};

        cc(lore, buy);

        inv.setItem(0, buy);


        ItemStack deny = new ItemStack(Material.STAINED_CLAY, 1, (short) 14);

        ItemStackUtil.setName(deny, "&6Sluit menu");

        inv.setItem(8, deny);


        ItemStack helmet = c.getHelmet();

        ItemStackUtil.setName(helmet, "&6" + c.getTitle() + " hoed");

        inv.setItem(13, helmet);


        ItemStack chest = c.getChestplate();

        ItemStackUtil.setName(chest, "&6" + c.getTitle() + " shirt");

        inv.setItem(22, chest);


        ItemStack legging = c.getLeggings();

        ItemStackUtil.setName(legging, "&6" + c.getTitle() + " broek");

        inv.setItem(31, legging);


        ItemStack boots = c.getBoots();

        ItemStackUtil.setName(boots, "&6" + c.getTitle() + " schoenen");

        inv.setItem(40, boots);


        return inv;

    }

}


