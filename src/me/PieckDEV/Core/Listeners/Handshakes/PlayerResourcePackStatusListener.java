/*    */
package me.PieckDEV.Core.Listeners.Handshakes;
/*    */ 
/*    */

import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Utils.ResourcepackUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerResourcePackStatusEvent;

/*    */
/*    */
/*    */
/*    */
/*    */
/*    */

/*    */
/*    */ 
/*    */ public class PlayerResourcePackStatusListener
/*    */ implements Listener
/*    */ {
    /*    */
    @EventHandler
/*    */ public void onResourcepackStatusEvent(PlayerResourcePackStatusEvent e)
/*    */ {
/* 18 */
        Player p = e.getPlayer();
/* 19 */
        if (e.getStatus() == PlayerResourcePackStatusEvent.Status.DECLINED)
/*    */ {
/* 21 */
            p.sendMessage(c("&7Het lijkt er op dat je de mogelijkheid om onze &3resourcepack &7automatisch te downloaden hebt uitgeschakeld. Schakel hem weer in voor de beste ervaring!"));
/* 22 */
            ResourcepackUtil.setStatus(p, false);
/*    */
        }
/* 24 */
        if (e.getStatus() == PlayerResourcePackStatusEvent.Status.FAILED_DOWNLOAD)
/*    */ {
/* 26 */
            p.sendMessage(c("&7Er is iets mis gegaan tijdens het downloaden van onze &3resourcepack&7! Probeer het opnieuw door opnieuw in te loggen of door &3/resourcepack &7te gebruiken!"));
/* 27 */
            ResourcepackUtil.setStatus(p, false);
/*    */
        }
/* 29 */
        if (e.getStatus() == PlayerResourcePackStatusEvent.Status.SUCCESSFULLY_LOADED)
/*    */ {
/* 31 */
            p.sendMessage(c("&7Je hebt succesvol onze resourcepack gedownload!"));
/* 32 */
            ResourcepackUtil.setStatus(p, true);
/*    */
        }
/*    */
    }

    /*    */
/*    */
    private String c(String string) {
/* 37 */
        return MS.c(string);
/*    */
    }
/*    */
}


/* Location:              C:\Users\Zorgklas 2\Desktop\PardoesCraft\plugins\PardoesCraft.jar!\me\CaptainXan\PardoesCraft\Listeners\PlayerResourcePackStatusListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */