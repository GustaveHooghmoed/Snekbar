package me.PieckDEV.Core.Rides.Events;


import me.CaptainXan.PardoesCraft.Rides.Enums.RideStatus;
import me.CaptainXan.PardoesCraft.Rides.Ride;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class RideStatusChangeEvent
        extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Ride ride;
    private RideStatus status;


    public RideStatusChangeEvent(Ride ride, RideStatus status) {

        this.ride = ride;

        this.status = status;

    }


    public static HandlerList getHandlerList() {

        return handlers;

    }


    public HandlerList getHandlers() {

        return handlers;

    }


    public RideStatus getStatus() {

        return this.status;

    }


    public Ride getRide() {

        return this.ride;

    }

}


