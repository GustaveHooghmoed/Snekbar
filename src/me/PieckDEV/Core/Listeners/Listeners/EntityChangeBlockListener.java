package me.PieckDEV.Core.Listeners.Listeners;


import org.bukkit.Material;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Wither;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;


public class EntityChangeBlockListener
        implements Listener {

    @EventHandler
    public void onChange(EntityChangeBlockEvent e) {

        Entity ent = e.getEntity();

        if ((ent instanceof EnderDragon)) {

            e.setCancelled(true);

        }

        if ((ent instanceof Wither)) {

            e.setCancelled(true);

        }

        if ((ent instanceof FallingBlock)) {

            FallingBlock fb = (FallingBlock) ent;

            if (fb.getMaterial() == Material.SPONGE) {

                e.setCancelled(true);

            }

        }

    }

}


