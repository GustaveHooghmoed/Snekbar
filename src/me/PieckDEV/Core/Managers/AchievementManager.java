package me.PieckDEV.Core.Managers;


import me.CaptainXan.PardoesCraft.Achievements.AchievementRangeManager;
import me.CaptainXan.PardoesCraft.Achievements.LocationRangeAchievement;
import me.CaptainXan.PardoesCraft.Main.Main;
import me.CaptainXan.PardoesCraft.Utils.ErrorUtil;
import me.CaptainXan.PardoesCraft.Utils.Objects.Achievement;
import me.IkeaKasten.Utils.MS;
import net.minecraft.server.v1_12_R1.EntityHuman;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.ArrayList;


public class AchievementManager {
    public static ArrayList<Achievement> achievements = new ArrayList();


    public static void setup() {

        // Setup LocationRangeAchievements

        String[] steenbokUncomp = {"Ik heb zin in...", "uhm.. een lekkere", "burger"};

        String[] steenbokComp = {"Wow, wat was dat", "een enorm lekkere burger!"};

        registerLocAchievement(new Achievement("steenbok1", "Een lekkere burger...", steenbokUncomp, steenbokComp, 5), new Location(Main.getWorld(), 241, 65, 555), 2D);






        String[] kluisUncomp = {"Slechts het ritme van een gouden hart", "opent deze kluis, een wonder start..."};

        String[] kluisComp = {"Je hebt met het ritme van je", "gouden hart de magische kluis geopend!"};

        registerAchievement(new Achievement("kluis", "Een gouden hart", kluisUncomp, kluisComp, 10));


        String[] aankoopUncomp = {"Over het hele park zijn er verschillende", "winkels te vinden. Misschien moet", "je met je dukaten iets leuks kopen?"};

        String[] aankoopComp = {"Je hebt je dukaten gespendeerd in een", "van onze winkels!"};

        registerAchievement(new Achievement("aankoop", "Mijn eerste aankoop", aankoopUncomp, aankoopComp, 5));


        String[] wensUncomp = {"Men zegt dat bij het werpen van een dukaat", "in dit water het geluk toe slaat!"};

        String[] wensComp = {"Je hebt een muntje in de wensbron", "geworpen, wat heb je gewenst?"};

        registerAchievement(new Achievement("wensbron", "Een bron vol wensen", wensUncomp, wensComp, 10));


        String[] wandelUncomp = {"Wandel 20 kilometer door ons park!"};

        String[] wandelComp = {"Je hebt 20 kilometer door ons park gewandeld", "tijd om te rusten! Of liever niet?"};

        registerAchievement(new Achievement("wandelen", "Tijd voor beweging", wandelUncomp, wandelComp, 15));


        String[] ezelUncomp = {"Wanneer de ezel zich strekt zal", "al gauw het goud worden verwerkt!"};

        String[] ezelComp = {"Je hebt de ezel doen strekken!"};

        registerAchievement(new Achievement("ezel", "Ezeltje, strek je!", ezelUncomp, ezelComp, 10));


        String[] audioUncomp = {"Verbind met onze audioserver door", "/audio te gebruiken!"};

        String[] audioComp = {"Je bent voor het eerst met onze", "audioserver verbonden!"};

        registerAchievement(new Achievement("audio", "Als muziek in de oren", audioUncomp, audioComp, 5));


        String[] tweedethuisUncomp = {"Breng meer dan 1 dag in het totaal", "door op PardoesCraft!"};

        String[] tweedethuisComp = {"Je hebt meer dan 1 dag in totaal", "doorgebracht op PardoesCraft!"};

        registerAchievement(new Achievement("tweedethuis", "Een tweede thuis", tweedethuisUncomp, tweedethuisComp, 20));


        String[] geluidsdichtUncomp = {"Het ziet er naar uit dat deze deur", "niet helemaal geluidsdicht is..."};

        String[] geluidsdichtComp = {"Het ziet er naar uit dat deze deur", "niet helemaal geluidsdicht is..."};

        registerAchievement(new Achievement("geluidsdicht", "Geen geluidsdichte deur", geluidsdichtUncomp, geluidsdichtComp, 10));


        String[] werkplaatsUncomp = {"Breng een bezoekje aan deze werkplaats!"};

        String[] werkplaatsComp = {"Je hebt een een kijkje genomen achter", "de schermen van de bobbaan!"};

        registerAchievement(new Achievement("werkplaats", "Zwitserse werkplaats", werkplaatsUncomp, werkplaatsComp, 10));


        String[] misselijkUncomp = {"Een rit in de verkeerde attractie", "kan je maag helemaal omdraaien..."};

        String[] misselijkComp = {"Je maag kon die ene rit niet aan", "waardoor je moest overgeven!"};

        registerAchievement(new Achievement("overgeven", "Omgedraaide maag", misselijkUncomp, misselijkComp, 15));


        String[] directieUncomp = {"Vind een directielid en raak hem/haar aan!"};

        String[] directieComp = {"Je hebt een directielid gevonden en", "hem/haar aangeraakt!"};

        registerAchievement(new Achievement("directie", "Magische aanraking", directieUncomp, directieComp, 10));


        String[] ballonUncomp = {"Stel zelf je eigen ballon samen", "in de Efteldingen!"};

        String[] ballonComp = {"Je hebt zelf je eigen ballon", "samengesteld in de Efteldingen!"};

        registerAchievement(new Achievement("ballon", "Eigen creatie", ballonUncomp, ballonComp, 10));

    }

    private static void registerLocAchievement(Achievement achievement, Location location, double v) {
        LocationRangeAchievement achievementloc = new LocationRangeAchievement(location, achievement, v);
        AchievementRangeManager.registerLocationRangeAchievement(achievementloc);
        if (!achievements.contains(achievement)) {

            achievements.add(achievement);

        }
    }


    public static void giveAchievement(Player p, Achievement ach) {

        if (!hasAchievement(p, ach)) {

            p.sendMessage(MS.c("&7----- &cAchievement&7-------"));

            p.sendMessage("");

            p.sendMessage(MS.c(" &c " + ach.getTitle()));

            p.sendMessage("");

            p.sendMessage(MS.c(" &7 Je bent beloond met &c" + ach.getReward() + " &7dukaten"));

            p.sendMessage("");

            p.sendMessage(MS.c("&7-----------------------"));


            totemEffect(p, false);


            File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/achievements.yml");

            YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

            ArrayList<String> list = (ArrayList) conf.getStringList("completed");


            list.add(ach.getName().toLowerCase());

            conf.set("completed", list);

            try {

                conf.save(f);

            } catch (Exception e) {

                ErrorUtil.reportException(e, AchievementManager.class);

            }

        }

    }


    public static void removeAchievement(Player p, Achievement ach) {

        if (hasAchievement(p, ach)) {

            File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/achievements.yml");

            YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

            ArrayList<String> list = (ArrayList) conf.getStringList("completed");


            list.remove(ach.getName().toLowerCase());

            conf.set("completed", list);

            try {

                conf.save(f);

            } catch (Exception e) {

                ErrorUtil.reportException(e, AchievementManager.class);

            }

        }

    }


    public static int percent(Player p) {

        double p1 = achievements.size();

        int p2 = getCompleted(p).size();

        if (p2 > 0) {

            double per2 = p2 / p1 * 100.0D;

            int per = (int) per2;

            return per;

        }

        return 0;

    }


    public static ArrayList<Achievement> getCompleted(Player p) {

        File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/achievements.yml");

        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

        ArrayList<String> list = (ArrayList) conf.getStringList("completed");


        ArrayList<Achievement> completed = new ArrayList();

        for (String name : list) {

            completed.add(getAchievementFromName(name));

        }

        return completed;

    }


    public static Achievement getAchievementFromName(String name) {

        for (Achievement ach : achievements) {

            if (ach.getName().equalsIgnoreCase(name)) {

                return ach;

            }

        }

        return null;

    }


    public static boolean hasAchievement(Player p, Achievement ach) {

        File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/achievements.yml");

        YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

        ArrayList<String> list = (ArrayList) conf.getStringList("completed");


        return list.contains(ach.getName().toLowerCase());

    }


    public static void setFile(Player p) {

        File f = new File("plugins/PardoesCraft/playerdata/" + p.getUniqueId() + "/achievements.yml");

        if (!f.exists()) {

            YamlConfiguration conf = YamlConfiguration.loadConfiguration(f);

            conf.set("completed", new ArrayList());

            try {

                conf.save(f);

            } catch (Exception e) {

                ErrorUtil.reportException(e, AchievementManager.class);

            }

        }

    }


    public static void registerAchievement(Achievement ach) {

        if (!achievements.contains(ach)) {

            achievements.add(ach);

        }

    }


    public static void totemEffect(Player p, boolean sound) {

        CraftPlayer cp = (CraftPlayer) p;

        EntityHuman eh = cp.getHandle();

        eh.world.broadcastEntityEffect(eh, (byte) 35);

        if (!sound) {

            p.stopSound(Sound.ITEM_TOTEM_USE);

        }

    }

}


