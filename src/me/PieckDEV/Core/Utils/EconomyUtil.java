package me.PieckDEV.Core.Utils;


import com.earth2me.essentials.api.Economy;
import me.CaptainXan.PardoesCraft.Utils.Guests.Guest;
import org.bukkit.entity.Player;


public class EconomyUtil {

    public static int getMoney(Player p) {

        int money = 0;

        try {

            money = (int) Economy.getMoney(p.getName());

        } catch (Exception e) {

            ErrorUtil.reportException(e, EconomyUtil.class);

        }

        return money;

    }


    public static void giveMoney(Player p, int amount) {

        try {

            Economy.add(p.getName(), amount);

        } catch (Exception e) {

            ErrorUtil.reportException(e, EconomyUtil.class);

        }

    }


    public static void takeMoney(Player p, int amount) {

        try {

            Economy.subtract(p.getName(), amount);

        } catch (Exception e) {

            ErrorUtil.reportException(e, EconomyUtil.class);

        }

    }


    public static boolean hasMoney(Player p, int amount) {

        return getMoney(p) >= amount;

    }


    public static int getScheduledMoney(Player p) {

        Guest g = new Guest(p);

        if (g.getGroup().getName().equalsIgnoreCase("Medewerker")) {

            return 10;

        }

        if (g.getGroup().getName().equalsIgnoreCase("Medewerker+")) {

            return 10;

        }

        if (p.isOp()) {

            return 10;

        }

        return 5;

    }

}


