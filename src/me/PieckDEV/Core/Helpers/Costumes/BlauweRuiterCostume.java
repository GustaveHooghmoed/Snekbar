package me.PieckDEV.Core.Helpers.Costumes;


import me.CaptainXan.PardoesCraft.Utils.Enums.CostumeType;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Costume;
import me.CaptainXan.PardoesCraft.Utils.ItemStackUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;


public class BlauweRuiterCostume
        implements Costume {

    public String getName() {

        return "blauweruiter";

    }


    public String getTitle() {

        return "Blauwe ruiter";

    }


    public ItemStack getHelmet() {

        ItemStack is = new ItemStack(Material.LEATHER_HELMET);

        ItemStackUtil.setLeatherRGB(is, 34, 201, 252);

        return is;

    }


    public ItemStack getChestplate() {

        ItemStack is = new ItemStack(Material.LEATHER_CHESTPLATE);

        ItemStackUtil.setLeatherRGB(is, 34, 201, 252);

        return is;

    }


    public ItemStack getLeggings() {

        ItemStack is = new ItemStack(Material.LEATHER_LEGGINGS);

        return is;

    }


    public ItemStack getBoots() {

        ItemStack is = new ItemStack(Material.LEATHER_BOOTS);

        ItemStackUtil.setLeatherRGB(is, 34, 201, 252);

        return is;

    }


    public CostumeType getType() {

        return CostumeType.GENERAL;

    }


    public int getPrice() {

        return 2500;

    }

}


