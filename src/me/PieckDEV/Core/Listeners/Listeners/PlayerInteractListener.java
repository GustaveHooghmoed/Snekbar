package me.PieckDEV.Core.Listeners.Listeners;


import me.CaptainXan.PardoesCraft.Menu.ParkMenu;
import me.IkeaKasten.Utils.MS;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;


public class PlayerInteractListener implements Listener {
    public static List<String> cooldowncake = new ArrayList();


    @EventHandler
    public void onInteract(PlayerInteractEvent e) {

        final Player p = e.getPlayer();

        EquipmentSlot hand = e.getHand();

        if ((hand == EquipmentSlot.HAND) && (
                (e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK))) {

            if (p.getEquipment().getItemInMainHand().getType() != Material.AIR) {

                ItemStack is = p.getEquipment().getItemInMainHand();

                if ((is.getType().toString().contains("CHESTPLATE")) || (is.getType().toString().contains("HELMET")) ||
                        (is.getType().toString().contains("LEGGINGS")) || (is.getType().toString().contains("BOOTS"))) {

                    e.setCancelled(true);

                    p.sendMessage(MS.c("&cGebruik het kostuum menu als je van kleding wilt veranderen!"));

                }

                if (is.hasItemMeta()) {

                    ItemMeta im = is.getItemMeta();

                    if ((is.getType() == Material.ENCHANTED_BOOK) &&
                            (im.getDisplayName().equalsIgnoreCase(MS.c("&3EftelMenu &7(Rechts klikken)")))) {

                        e.setCancelled(true);

                        p.openInventory(ParkMenu.parkmenu(p));

                    }

                }

            }

        }

    }

}


