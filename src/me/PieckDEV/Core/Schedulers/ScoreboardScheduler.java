package me.PieckDEV.Core.Schedulers;


import me.CaptainXan.PardoesCraft.Utils.Guests.Guest;
import me.IkeaKasten.Utils.MS;
import me.PieckDEV.Core.Utils.MessageUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.ArrayList;
import java.util.List;


public class ScoreboardScheduler
        extends BukkitRunnable {
    private static List<Player> players = new ArrayList();


    public static void setScoreboard(Player p) {

        ScoreboardManager manager = Bukkit.getScoreboardManager();

        Scoreboard board = manager.getNewScoreboard();

        Objective objective = board.registerNewObjective("main", "dummy");


        objective.setDisplayName(MessageUtil.getTitle());

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        p.setScoreboard(board);

        players.add(p);

    }


    public void run() {

        for (Player p : Bukkit.getOnlinePlayers()) {

            if (players.contains(p)) {

                Guest g = new Guest(p);

                Scoreboard board = g.getPlayer().getScoreboard();

                for (Objective obj : board.getObjectives()) {

                    obj.getScore(MS.c("&7Online spelers")).setScore(Bukkit.getOnlinePlayers().size());

                    obj.getScore(MS.c("&7Unieke bezoekers")).setScore(Bukkit.getOfflinePlayers().length);

                    obj.getScore(MS.c("&7Dukaten")).setScore((int)g.getMoney());


                    p.setScoreboard(board);

                }

            } else {

                setScoreboard(p);

            }

        }

    }

}


