package me.PieckDEV.Core.Utils;


import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.ArrayList;


public class ShapeUtil {

    public static ArrayList<Location> line(Location from, Location to) {

        ArrayList<Location> locs = new ArrayList();

        float yaw = LocationUtil.getYawTo(from, to);

        float pitch = LocationUtil.getPitchTo(from, to);

        Location origin = new Location(from.getWorld(), from.getX(), from.getY(), from.getZ(), yaw - 270.0F, pitch - 270.0F);

        Vector step = origin.getDirection().multiply(0.5D);

        for (int i = 0; i < from.distance(to) / 2.0D; i++) {

            origin.add(step);

            locs.add(new Location(from.getWorld(), origin.getX(), origin.getY(), origin.getZ()));

        }

        return locs;

    }


    public static ArrayList<Location> blocksphere(Location location, int radius) {

        ArrayList<Location> blocks = new ArrayList();

        World world = location.getWorld();

        int X = location.getBlockX();

        int Y = location.getBlockY();

        int Z = location.getBlockZ();

        int radiusSquared = radius * radius;

        for (int x = X - radius; x <= X + radius; x++) {

            for (int y = Y - radius; y <= Y + radius; y++) {

                for (int z = Z - radius; z <= Z + radius; z++) {

                    if ((X - x) * (X - x) + (Y - y) * (Y - y) + (Z - z) * (Z - z) <= radiusSquared) {

                        Location block = new Location(world, x, y, z);

                        blocks.add(block);

                    }

                }

            }

        }

        return blocks;

    }


    public static ArrayList<Location> sphere(Location loc, double radius) {

        ArrayList<Location> locs = new ArrayList();


        double locations = radius * 150.0D;

        double offset = 2.0D / locations;

        double increment = 3.141592653589793D * (3.0D - Math.sqrt(5.0D));

        for (int i = 0; i < locations; i++) {

            double y = i * offset - 1.0D + offset / 2.0D;

            double r = Math.sqrt(1.0D - Math.pow(y, 2.0D));

            double phi = (i + 1.0D) % locations * increment;

            double x = Math.cos(phi) * r;

            double z = Math.sin(phi) * r;

            locs.add(loc.clone().add(x * radius, y * radius, z * radius));

        }

        return locs;

    }

}


