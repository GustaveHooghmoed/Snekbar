package me.PieckDEV.Core.Utils;


import me.CaptainXan.PardoesCraft.Main.Main;
import me.PieckDEV.Core.Utils.Objects.Point;
import net.minecraft.server.v1_12_R1.EntityLiving;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftEntity;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class LocationUtil {

    public static Location MonsieurRela(Location center, double distance, double speed, double radius, double angle) {

        double yaw = Math.toRadians(angle);

        double x = center.getX() + distance * Math.sin(yaw);

        double z = center.getZ() + distance * Math.sin(yaw);

        return new Location(Main.world, x, 65.0D, z);

    }


    public static Location getRelative(Location center, double distance, double angle) {

        double yaw = Math.toRadians(angle);


        double x = center.getX() + distance * Math.cos(yaw);

        double z = center.getZ() + distance * Math.sin(yaw);


        Location to = new Location(Main.world, x, center.getY(), z);


        return to;

    }


    public static float getAngle(Vector point1, Vector point2) {

        double dx = point2.getX() - point1.getX();

        double dz = point2.getZ() - point1.getZ();

        float angle = (float) Math.toDegrees(Math.atan2(dz, dx)) - 90.0F;

        if (angle < 0.0F) {

            angle += 360.0F;

        }

        return angle;

    }


    public static String getNearestWarp(Player p) {

        HashMap<Double, String> warps = new HashMap();

        ArrayList<Double> distance = new ArrayList();

        for (String warp : Main.ess.getWarps().getList()) {

            if (p.hasPermission("essentials.warps." + warp.toLowerCase())) {

                try {

                    warps.put(Double.valueOf(Main.ess.getWarps().getWarp(warp).distance(p.getLocation())), warp.toLowerCase());

                    distance.add(Double.valueOf(Main.ess.getWarps().getWarp(warp).distance(p.getLocation())));

                } catch (Exception e) {

                    e.printStackTrace();

                }

            }

        }


        if (warps.size() > 0) {

            return warps.get(Collections.min(distance));

        }

        return "spawn";

    }


    public static Location keepYawAndPitch(Location loc, Location keep) {

        return new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ(), keep.getYaw(), keep.getPitch());

    }


    public static ArrayList<Location> getCircleLocs(Location center, double radius, int amount) {

        World world = center.getWorld();

        double increment = 6.283185307179586D / amount;

        ArrayList<Location> locations = new ArrayList();

        for (int i = 0; i < amount; i++) {

            double angle = i * increment;

            double x = center.getX() + radius * Math.cos(angle);

            double z = center.getZ() + radius * Math.sin(angle);

            locations.add(new Location(world, x, center.getY(), z));

        }

        return locations;

    }


    public static boolean rangeContainsPlayers(Location origin, int radius) {

        ArrayList<Player> players = new ArrayList();

        for (org.bukkit.entity.Entity e : getTargetList(origin, radius)) {

            if ((e instanceof Player)) {

                players.add((Player) e);

            }

        }

        return players.size() > 0;

    }


    public static ArrayList<Location> getLocsTo(Player p, ArmorStand from, ArmorStand to, String timeString) {

        Date length = null;

        try {

            length = parseTimeString(timeString);

        } catch (ParseException e1) {

            e1.printStackTrace();

        }

        Calendar cal = java.util.GregorianCalendar.getInstance();

        cal.setTime(length);


        int time = (cal.get(12) * 60 + cal.get(13)) * 20;


        ArrayList<ArmorStand> locs = new ArrayList();

        locs.add(from);

        locs.add(to);

        ArrayList<Double> diffs = new ArrayList();

        ArrayList<Integer> travelTimes = new ArrayList();


        double totalDiff = 0.0D;

        for (int i = 0; i < locs.size() - 1; i++) {

            ArmorStand s = locs.get(i);

            ArmorStand n = locs.get(i + 1);

            double diff = positionDifference(s.getLocation(), n.getLocation());

            totalDiff += diff;

            diffs.add(Double.valueOf(diff));

        }

        double d = 0.5D;

        travelTimes.add(Integer.valueOf((int) (d / totalDiff * time)));

        ArrayList<Location> tps = new ArrayList();


        World w = p.getWorld();

        for (int i = 0; i < locs.size() - 1; i++) {

            ArmorStand s = locs.get(i);

            ArmorStand n = locs.get(i + 1);

            int t = travelTimes.get(i).intValue();


            double moveX = n.getLocation().getX() - s.getLocation().getX();

            double moveY = n.getLocation().getY() - s.getLocation().getY();

            double moveZ = n.getLocation().getZ() - s.getLocation().getZ();

            double movePitch = Math.toRadians(n.getHeadPose().getX()) - Math.toRadians(s.getHeadPose().getX());


            double yawDiff = Math.abs(Math.toRadians(n.getHeadPose().getY()) - Math.toRadians(s.getHeadPose().getY()));

            double c = 0.0D;

            if (yawDiff <= 180.0D) {

                if (Math.toRadians(s.getHeadPose().getY()) < Math.toRadians(n.getHeadPose().getY())) {

                    c = yawDiff;

                } else {

                    c = -yawDiff;

                }

            } else if (Math.toRadians(s.getHeadPose().getY()) < Math.toRadians(n.getHeadPose().getY())) {

                c = -(360.0D - yawDiff);

            } else {

                c = 360.0D - yawDiff;

            }

            double d1 = c / t;

            for (int x = 0; x < t; x++) {

                Location l = new Location(w, s.getLocation().getX() + moveX / t * x, s.getLocation().getY() + moveY / t * x, s.getLocation().getZ() + moveZ / t * x, (float) (Math.toRadians(s.getHeadPose().getY()) + d1 * x), (float) (Math.toRadians(s.getHeadPose().getX()) + movePitch / t * x));

                tps.add(l);

            }

        }

        return tps;

    }


    public static Date parseTimeString(String timeString)
            throws ParseException {

        Date length;

        try {

            SimpleDateFormat formatter = new SimpleDateFormat("mm'm'ss's'");

            length = formatter.parse(timeString);

        } catch (Exception e) {

            try {

                SimpleDateFormat formatter = new SimpleDateFormat("m'm'ss's'");

                length = formatter.parse(timeString);

            } catch (Exception e1) {

                try {

                    SimpleDateFormat formatter = new SimpleDateFormat("m'm's's'");

                    length = formatter.parse(timeString);

                } catch (Exception e2) {

                    try {

                        SimpleDateFormat formatter = new SimpleDateFormat("mm'm's's'");

                        length = formatter.parse(timeString);

                    } catch (Exception e3) {

                        try {

                            SimpleDateFormat formatter = new SimpleDateFormat("mm'm'");

                            length = formatter.parse(timeString);

                        } catch (Exception e4) {

                            try {

                                SimpleDateFormat formatter = new SimpleDateFormat("m'm'");

                                length = formatter.parse(timeString);

                            } catch (Exception e5) {

                                try {

                                    SimpleDateFormat formatter = new SimpleDateFormat("s's'");

                                    length = formatter.parse(timeString);

                                } catch (Exception e6) {

                                    SimpleDateFormat formatter = new SimpleDateFormat("ss's'");

                                    length = formatter.parse(timeString);

                                }

                            }

                        }

                    }

                }

            }

        }

        return length;

    }


    public static double positionDifference(Location cLoc, Location eLoc) {

        double cX = cLoc.getX();

        double cY = cLoc.getY();

        double cZ = cLoc.getZ();


        double eX = eLoc.getX();

        double eY = eLoc.getY();

        double eZ = eLoc.getZ();


        double dX = eX - cX;

        if (dX < 0.0D) {

            dX = -dX;

        }

        double dZ = eZ - cZ;

        if (dZ < 0.0D) {

            dZ = -dZ;

        }

        double dXZ = Math.hypot(dX, dZ);


        double dY = eY - cY;

        if (dY < 0.0D) {

            dY = -dY;

        }

        double dXYZ = Math.hypot(dXZ, dY);


        return dXYZ;

    }


    public static void lookAtBlock2(Location loc, org.bukkit.entity.Entity e) {

        Vector direction = getVector(e.getLocation()).subtract(getVector(loc)).normalize();

        double x = direction.getX();

        double z = direction.getZ();


        setM(e, 90.0F - toDegree(Math.atan2(x, z)));

    }


    public static void lookAtBlock(Location loc, org.bukkit.entity.Entity e) {

        Vector direction = getVector(e.getLocation()).subtract(getVector(loc)).normalize();

        double x = direction.getX();

        double z = direction.getZ();


        setM(e, 180.0F - toDegree(Math.atan2(x, z)));

    }


    public static float clampYaw(float yaw) {

        while (yaw < -180.0F) {

            yaw += 360.0F;

        }

        while (yaw >= 180.0F) {

            yaw -= 360.0F;

        }

        return yaw;

    }


    public static void setM(org.bukkit.entity.Entity target, float yaw) {

        net.minecraft.server.v1_12_R1.Entity cart2 = ((CraftEntity) target).getHandle();

        cart2.yaw = yaw;

        if (!(target instanceof EntityLiving)) {

            return;

        }

        EntityLiving handle = (EntityLiving) target;

        yaw = clampYaw(yaw);

        handle.aO = yaw;

        if (!(handle instanceof net.minecraft.server.v1_12_R1.EntityHuman)) {

            handle.aM = yaw;

        }

        handle.aP = yaw;

    }


    public static void setPitch(org.bukkit.entity.Entity target, float pitch) {

        net.minecraft.server.v1_12_R1.Entity cart2 = ((CraftEntity) target).getHandle();

        cart2.pitch = pitch;

    }


    private static float toDegree(double angle) {

        return (float) Math.toDegrees(angle);

    }


    public static Location lookAt(Location from, Location to) {

        Vector direction = getVector(from).subtract(getVector(to)).normalize();

        double x = direction.getX();

        double y = direction.getY();

        double z = direction.getZ();


        from.setYaw(toDegree(Math.atan2(x, z)));

        from.setPitch(toDegree(Math.acos(y)));


        return from;

    }


    public static float getPitchTo(Location from, Location to) {

        Location fromclone = from.clone();

        Location toclone = to.clone();

        fromclone.setDirection(toclone.subtract(fromclone.toVector()).toVector());

        return fromclone.getPitch();

    }


    public static float getYawTo(Location from, Location to) {

        Location fromclone = from.clone();

        Location toclone = to.clone();

        fromclone.setDirection(toclone.subtract(fromclone.toVector()).toVector());

        return fromclone.getYaw();

    }


    public static boolean enoughSpace(Location loc, int radius) {

        ArrayList<Location> lo = new ArrayList();

        for (Location locs : ShapeUtil.blocksphere(loc, radius)) {

            if (locs.getBlock().getType() == Material.AIR) {

                lo.add(locs);

            }

        }

        return lo.size() == ShapeUtil.blocksphere(loc, radius).size();

    }


    private static Vector getVector(Location loc) {

        return loc.toVector();

    }


    public static Location getLocationFromCoords(int x, int y, int z) {

        return Main.world.getBlockAt(x, y, z).getLocation().add(0.5D, 0.0D, 0.5D);

    }


    public static List<org.bukkit.entity.Entity> getTargetList(Location loc, int radius) {

        List<org.bukkit.entity.Entity> target = new ArrayList();

        int rs = radius * radius;

        Location tmp = new Location(loc.getWorld(), 0.0D, 0.0D, 0.0D);

        if (loc.getWorld().getEntities().size() > 0) {

            for (org.bukkit.entity.Entity entity : loc.getWorld().getEntities()) {

                if (entity.getLocation(tmp).distanceSquared(loc) < rs) {

                    target.add(entity);

                }

            }

        }

        return target;

    }


    public static Vector getFromTo(Location locFrom, Location locTo) {

        return locTo.toVector().subtract(locFrom.toVector());

    }


    public static List<Location> transformPoints(Location center, List<Point> points, double yaw, double pitch, double roll, double scale) {

        yaw = Math.toRadians(yaw);

        pitch = Math.toRadians(pitch);

        roll = Math.toRadians(roll);

        List<Location> list = new ArrayList();


        double cp = Math.cos(pitch);

        double sp = Math.sin(pitch);

        double cy = Math.cos(yaw);

        double sy = Math.sin(yaw);

        double cr = Math.cos(roll);

        double sr = Math.sin(roll);

        for (Point point : points) {

            double x = point.getX();

            double bx = x;

            double y = point.getY();

            double by = y;

            double z = point.getZ();

            double bz = z;

            x = ((x * cy - bz * sy) * cr + by * sr) * scale;

            y = ((y * cp + bz * sp) * cr - bx * sr) * scale;

            z = ((z * cp - by * sp) * cy + bx * sy) * scale;

            list.add(new Location(center.getWorld(), center.getX() + x, center.getY() + y, center.getZ() + z));

        }

        return list;

    }

}


