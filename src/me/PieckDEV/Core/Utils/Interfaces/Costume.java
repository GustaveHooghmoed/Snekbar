package me.PieckDEV.Core.Utils.Interfaces;

import me.PieckDEV.Core.Utils.Enums.CostumeType;
import org.bukkit.inventory.ItemStack;

public interface Costume {
    String getName();

    String getTitle();

    ItemStack getHelmet();

    ItemStack getChestplate();

    ItemStack getLeggings();

    ItemStack getBoots();

    CostumeType getType();

    int getPrice();
}


