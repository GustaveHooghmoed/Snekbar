/*    */
package me.PieckDEV.Core.Utils;
/*    */ 
/*    */

import com.nametagedit.plugin.NametagEdit;
import me.CaptainXan.PardoesCraft.Main.Main;
import me.IkeaKasten.Utils.MS;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/*    */
/*    */
/*    */
/*    */
/*    */
/*    */
/*    */

/*    */
/*    */ 
/*    */ public class NameUtil
/*    */ {
    /*    */
    public static void setTabName(Player p, final String name)
/*    */ {
/*    */
        try
/*    */ {
/* 19 */
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.pl, new Runnable()
/*    */ {
                /*    */
                public void run()
/*    */ {
/* 23 */
                    p.setPlayerListName(name);
/*    */
                }
/* 25 */
            }, 2L);
/*    */
        }
/*    */ catch (Exception e)
/*    */ {
/* 29 */
            ErrorUtil.reportException(e, NameUtil.class);
/*    */
        }
/*    */
    }

    /*    */
/*    */
    public static void setPrefix(Player p, final String name)
/*    */ {
/* 35 */
        if (name != null) {
/*    */
            try
/*    */ {
/* 38 */
                Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.pl, new Runnable()
/*    */ {
                    /*    */
                    public void run()
/*    */ {
/* 42 */
                        NametagEdit.getApi().setPrefix(p, MS.c(name));
/*    */
                    }
/* 44 */
                }, 3L);
/*    */
            }
/*    */ catch (Exception e)
/*    */ {
/* 48 */
                e.printStackTrace();
/* 49 */
                ErrorUtil.reportException(e, NameUtil.class);
/*    */
            }
/*    */
        } else {
/* 52 */
            Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Main.pl, new Runnable()
/*    */ {
                /*    */
                public void run()
/*    */ {
/* 56 */
                    NametagEdit.getApi().setPrefix(p, MS.c("&7"));
/*    */
                }
/* 58 */
            }, 2L);
/*    */
        }
/*    */
    }
/*    */
}


/* Location:              C:\Users\Zorgklas 2\Desktop\PardoesCraft\plugins\PardoesCraft.jar!\me\CaptainXan\PardoesCraft\Utils\NameUtil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */