package PathAPI.main;


public class Cubic {
    private float a;
    private float b;
    private float c;
    private float d;


    public Cubic(float a, float b, float c, float d) {

        this.a = a;

        this.b = b;

        this.c = c;

        this.d = d;

    }


    public float eval(float u) {

        return ((this.d * u + this.c) * u + this.b) * u + this.a;

    }

}


