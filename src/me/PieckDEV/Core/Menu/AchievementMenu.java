package me.PieckDEV.Core.Menu;


import me.PieckDEV.Core.Managers.AchievementManager;
import me.PieckDEV.Core.Utils.ItemStackUtil;
import me.PieckDEV.Core.Utils.MenuUtil;
import me.PieckDEV.Core.Utils.Objects.Achievement;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


public class AchievementMenu {

    public static Inventory achievementmenu(Player p) {

        Inventory inv = MenuUtil.menu(c("        &8&nAchievements  ") + AchievementManager.percent(p) + "%", 3);

        for (int i = 9; i - 9 < AchievementManager.achievements.size(); i++) {

            inv.setItem(i, achItem(AchievementManager.achievements.get(i - 9), p));

        }

        return inv;

    }


    private static ItemStack achItem(Achievement a, Player p) {

        ItemStack item = new ItemStack(Material.STAINED_CLAY, 1, achColorValue(a, p));

        String[] lore = Desc(a, p);

        ItemStackUtil.setName(item, c("&6Achievement: &e" + a.getTitle()));

        ItemStackUtil.setLore(item, c(lore));

        return item;

    }


    private static String[] Desc(Achievement a, Player p) {

        if (AchievementManager.hasAchievement(p, a)) {

            return c(a.getCompletedDescription());

        }

        return c(a.getUncompletedDescription());

    }


    private static String[] c(String[] xd) {

        StringBuilder sb = new StringBuilder();

        String[] arrayOfString = xd;
        int j = xd.length;
        for (int i = 0; i < j; i++) {
            String s = arrayOfString[i];

            String b = c(s);

            sb.append(b + ";");

        }

        return sb.toString().split(";");

    }


    private static short achColorValue(Achievement a, Player p) {

        int i = 9;

        if (AchievementManager.hasAchievement(p, a)) {

            return 5;

        }

        return 14;

    }


    private static String c(String string) {

        return ChatColor.translateAlternateColorCodes('&', "&7" + string);

    }

}


