package me.PieckDEV.Core.Utils.RawMessage;


import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;


public class JSONChatMessage {
    private JSONObject chatObject;


    public JSONChatMessage(String text, JSONChatColor color, List<JSONChatFormat> formats) {

        this.chatObject = new JSONObject();

        this.chatObject.put("text", text);

        if (color != null) {

            this.chatObject.put("color", color.getColorString());

        }

        if (formats != null) {

            for (JSONChatFormat format : formats) {

                this.chatObject.put(format.getFormatString(), Boolean.valueOf(true));

            }

        }

    }


    public void addExtra(JSONChatExtra extraObject) {

        if (!this.chatObject.containsKey("extra")) {

            this.chatObject.put("extra", new JSONArray());

        }

        JSONArray extra = (JSONArray) this.chatObject.get("extra");

        extra.add(extraObject.toJSON());

        this.chatObject.put("extra", extra);

    }


    public void sendToPlayer(Player player) {

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.a(this.chatObject.toJSONString())));

    }


    public String toString() {

        return this.chatObject.toJSONString();

    }

}


