package me.PieckDEV.Core.Utils.Relatives;


import org.bukkit.entity.ArmorStand;
import org.bukkit.util.Vector;


public class StandRelative {
    private ArmorStand center;
    private ArmorStand secondary;
    private double distance = 0.0D;
    private double angle = 0.0D;
    private double ydiff = 0.0D;


    public StandRelative(ArmorStand center, ArmorStand secondary) {

        this.center = center;

        this.secondary = secondary;


        double distance = center.getLocation().distance(secondary.getLocation());

        double angle = calculateAngle(center.getLocation().toVector(), secondary.getLocation().toVector()) - 270.0F - center.getLocation().getYaw();

        double ydiff = center.getLocation().getY() - secondary.getLocation().getY();


        this.ydiff = ydiff;

        this.distance = distance;

        this.angle = angle;

    }


    public ArmorStand getCenterStand() {

        return this.center;

    }


    public ArmorStand getSecondaryStand() {

        return this.secondary;

    }


    public double getDistance() {

        return this.distance;

    }


    public void setDistance(double value) {

        this.distance = value;

    }


    public double getAngle() {

        return this.angle;

    }


    public double getYDifference() {

        return this.ydiff;

    }


    private float calculateAngle(Vector point1, Vector point2) {

        double dx = point2.getX() - point1.getX();

        double dz = point2.getZ() - point1.getZ();

        float angle = (float) Math.toDegrees(Math.atan2(dz, dx)) - 90.0F;

        if (angle < 0.0F) {

            angle += 360.0F;

        }

        return angle;

    }

}


