package me.PieckDEV.Core.Utils.Interfaces;

public interface RideScene {
    void start();

    boolean isStarted();

    void setStarted(boolean paramBoolean);

    String getName();
}


