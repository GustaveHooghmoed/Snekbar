package me.PieckDEV.Core.Main;


import com.earth2me.essentials.Essentials;
import me.CaptainXan.PardoesCraft.Commands.*;
import me.CaptainXan.PardoesCraft.Listeners.Handshakes.PlayerResourcePackStatusListener;
import me.CaptainXan.PardoesCraft.Listeners.Listeners.*;
import me.CaptainXan.PardoesCraft.Listeners.Updates.UpdateListener;
import me.IkeaKasten.Utils.Broadcast;
import me.PieckDEV.Core.Commands.AchievementCommand;
import me.PieckDEV.Core.Commands.ResourcepackCommand;
import me.PieckDEV.Core.Commands.RideSceneCommand;
import me.PieckDEV.Core.Commands.RideStatusCommand;
import me.PieckDEV.Core.Listeners.Handshakes.AfkStatusChangeListener;
import me.PieckDEV.Core.Listeners.Handshakes.PingEventListener;
import me.PieckDEV.Core.Listeners.Listeners.*;
import me.PieckDEV.Core.Listeners.Updates.CostumeParticles;
import me.PieckDEV.Core.Managers.AchievementManager;
import me.PieckDEV.Core.Managers.CostumeManager;
import me.PieckDEV.Core.Managers.DoorManager;
import me.PieckDEV.Core.Managers.RideManager;
import me.PieckDEV.Core.Schedulers.MoneyScheduler;
import me.PieckDEV.Core.Schedulers.ScoreboardScheduler;
import me.PieckDEV.Core.Utils.ErrorUtil;
import me.PieckDEV.Core.Utils.UpdateUtil;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public class Main extends JavaPlugin {
    public static Main pl;
    public static Essentials ess;
    public static World world;


    public void onEnable() {

        Broadcast.withPerm("&cPieckCraft &8 >> &7De core wordt ingeschakeld...", "PC.Admin");

        try {

            pl = this;

            ess = (Essentials) Bukkit.getPluginManager().getPlugin("Essentials");

            world = Bukkit.getWorld("world");

            RideManager.setup();

            AchievementManager.setup();

            CostumeManager.setup();

            UpdateUtil.setup();


            registerListeners();

            registerRunnables();

            registerCommands();

            registerRides();
        } catch (Exception e) {

            ErrorUtil.reportException(e, pl.getClass());

        }

    }


    public void onDisable() {
        Broadcast.withPerm("&cPieckCraft &8 >> &7De core wordt uitgeschakeld...", "PC.Admin");
        DoorManager.despawn();
        pl = null;




    }


    private void registerRides() {
    }


    private void registerCommands() {

        pl.getCommand("status").setExecutor(new RideStatusCommand());

        pl.getCommand("achievement").setExecutor(new AchievementCommand());

        pl.getCommand("PieckCraft").setExecutor(new PieckCraftCommand());

        pl.getCommand("resourcepack").setExecutor(new ResourcepackCommand());

        pl.getCommand("ridescene").setExecutor(new RideSceneCommand());


    }


    private void registerRunnables() {

        new ScoreboardScheduler().runTaskTimer(pl, 0L, 20L);

        new MoneyScheduler().runTaskTimer(pl, 0L, 6000L);

    }


    private void registerListeners() {

        PluginManager plm = Bukkit.getPluginManager();

        plm.registerEvents(new me.CaptainXan.PardoesCraft.Listeners.Handshakes.PlayerJoinListener(), pl);

        plm.registerEvents(new me.CaptainXan.PardoesCraft.Listeners.Handshakes.PlayerQuitListener(), pl);

        plm.registerEvents(new PlayerInteractListener(), pl);

        plm.registerEvents(new InventoryClickListener(), pl);

        plm.registerEvents(new RideStatusChangeListener(), pl);


        plm.registerEvents(new UpdateListener(), pl);

        plm.registerEvents(new AfkStatusChangeListener(), pl);

        plm.registerEvents(new PermissionEntityListener(), pl);

        plm.registerEvents(new PlayerInteractAtEntityListener(), pl);

        plm.registerEvents(new CostumeParticles(), pl);


        plm.registerEvents(new PlayerResourcePackStatusListener(), pl);


        plm.registerEvents(new PlayerDropItemListener(), pl);

        plm.registerEvents(new EntityChangeBlockListener(), pl);

        plm.registerEvents(new RideFix(), pl);

        plm.registerEvents(new PingEventListener(), pl);

    }

    public static Main getPl() {
        return pl;
    }

    public static World getWorld() {
        return world;
    }

    public static Essentials getEss() {
        return ess;
    }


}

