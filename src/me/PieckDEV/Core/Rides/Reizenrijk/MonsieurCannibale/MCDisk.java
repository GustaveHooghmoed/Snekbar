package me.PieckDEV.Core.Rides.Reizenrijk.MonsieurCannibale;


import me.CaptainXan.PardoesCraft.Main.Main;
import me.PieckDEV.Core.Utils.LocationUtil;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;


public class MCDisk {
    private ArrayList<MCCup> cups = new ArrayList();
    private double angle = 0.0D;
    private double speed = 0.0D;
    private boolean changed = false;


    public MCDisk(Location location) {

        ArrayList<Location> locs = LocationUtil.getCircleLocs(location, 2.6D, 3);

        for (Location loc : locs) {

            MCCup cup = new MCCup(loc, (short) (locs.indexOf(loc) + 7));

            this.cups.add(cup);

        }

    }


    public void moveTo(Location location) {

        for (int i = 0; i < this.cups.size(); i++) {

            MCCup cup = this.cups.get(i);

            double add = 360 / this.cups.size() * i;

            Location to = LocationUtil.getRelative(location, 2.6D, this.angle + add);

            cup.moveTo(to);

        }

    }


    public void remove() {

        for (MCCup cup : this.cups) {

            cup.remove();

        }

    }


    public double getSpeed() {

        return this.speed;

    }


    public void setSpeed(final double tospeed) {

        if (tospeed != 0.0D) {

            this.speed = tospeed;

            if (!this.changed) {

                this.changed = true;


                new BukkitRunnable() {

                    public void run() {

                        if (tospeed != 0.0D) {

                            MCDisk.this.angle += MCDisk.this.speed;

                        } else {

                            cancel();

                        }

                    }

                }.runTaskTimer(Main.pl, 0L, 1L);

            }

        }

    }


    public ArrayList<MCCup> getCups() {

        return this.cups;

    }

}


