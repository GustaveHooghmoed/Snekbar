package me.PieckDEV.Core.Utils.RawMessage;


public enum JSONChatFormat {
    BOLD("bold"), UNDERLINED("underlined"), STRIKETHROUGH("strikethrough"), ITALIC("italic"), NORMAL("normal"), OBFUSCATED("obfuscated");

    private final String format;


    JSONChatFormat(String format) {

        this.format = format;

    }


    public String getFormatString() {

        return this.format;

    }

}


