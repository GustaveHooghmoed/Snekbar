package me.PieckDEV.Core.Utils.Objects;


import me.CaptainXan.PardoesCraft.Main.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.FallingBlock;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;


public class BloomJet {
    public Location location;
    public double size;
    public double height;


    public BloomJet(Location location, double size, double height) {

        this.location = location;

        this.size = size;

        this.height = height;

    }


    public static ArrayList<Vector> getCirclePoints(Vector center, double radius, int total) {

        center = new Vector(0, 0, 0);

        ArrayList<Vector> points = new ArrayList(total);

        double interval = 6.283185307179586D / total;

        for (double currentAngle = 6.283185307179586D; currentAngle > 0.0D; currentAngle -= interval) {

            points.add(new Vector(center.getX() + radius * Math.sin(currentAngle), center.getY(), center.getZ() + radius *
                    Math.cos(currentAngle)));

        }

        return points;

    }


    public void execute() {

        final int amount = (int) (this.size * 6.0D);


        new BukkitRunnable() {
            int timer = 0;


            public void run() {

                if (this.timer < 3) {

                    for (int i = 0; i < amount; i++) {

                        FallingBlock fb = Main.world.spawnFallingBlock(BloomJet.this.location, Material.SPONGE, (byte) 0);

                        fb.setDropItem(false);

                        ArrayList<Vector> circle = BloomJet.getCirclePoints(BloomJet.this.location.toVector(), BloomJet.this.size, amount);

                        fb.setVelocity(new Vector(circle.get(i).getX() * 0.1D, BloomJet.this.height, circle.get(i).getZ() * 0.1D));

                    }

                    this.timer += 1;

                } else {

                    cancel();

                }

            }

        }.runTaskTimer(Main.pl, 0L, 3L);

    }

}


