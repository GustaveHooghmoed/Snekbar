package me.PieckDEV.Core.Rides.Reizenrijk.MonsieurCannibale;


import me.CaptainXan.PardoesCraft.Main.Main;
import me.CaptainXan.PardoesCraft.Utils.Interfaces.Flatride;
import me.PieckDEV.Core.Utils.LocationUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Iterator;


public class MonsieurCannibale
        implements Flatride {
    private boolean started = false;
    private ArrayList<MCDisk> disks = new ArrayList();
    private Location center = new Location(Main.world, 803.0D, 64.0D, -399.0D).add(0.5D, -0.4D, 0.5D);
    private double angle = 0.0D;
    private double speed = 0.0D;


    public boolean isStarted() {

        return this.started;

    }


    public String getName() {

        return "mc";

    }


    public void spawn() {

        MCCup.setDistance(1.0D);

        for (Location loc : LocationUtil.getCircleLocs(this.center, 7.0D, 4)) {

            MCDisk disk = new MCDisk(loc);

            Bukkit.broadcastMessage(loc.toString());

            this.disks.add(disk);

        }

    }


    public void despawn() {

        for (MCDisk disk : this.disks) {

            disk.remove();

        }

    }


    public void start() {

        MCCup.setDistance(1.0D);

        if (!isStarted()) {

            this.started = true;

            setSpeed(0.02D);

            new BukkitRunnable() {

                public void run() {

                    if (MonsieurCannibale.this.speed < 2.0D) {

                        MonsieurCannibale.this.speed += 0.01D;

                        MonsieurCannibale.this.setSpeed(MonsieurCannibale.this.speed);

                        Iterator localIterator2;
                        for (Iterator localIterator1 = MonsieurCannibale.this.disks.iterator(); localIterator1.hasNext();

                             localIterator2.hasNext()) {

                            MCDisk disk = (MCDisk) localIterator1.next();

                            disk.setSpeed(-MonsieurCannibale.this.speed);

                            localIterator2 = disk.getCups().iterator();
                            MCCup cup = (MCCup) localIterator2.next();

                            cup.setSpeed(MonsieurCannibale.this.speed);

                        }


                    } else {

                        cancel();

                    }

                }

            }.runTaskTimer(Main.pl, 0L, 1L);


            new BukkitRunnable() {

                public void run() {

                    if (MonsieurCannibale.this.speed > 0.0D) {

                        MonsieurCannibale.this.angle += MonsieurCannibale.this.speed;

                        MonsieurCannibale.this.update();

                    } else {

                        cancel();

                    }

                }

            }.runTaskTimer(Main.pl, 0L, 1L);


            new BukkitRunnable() {
                public int timer = 0;


                public void run() {

                    if (this.timer < 800) {

                        this.timer += 1;

                    } else {

                        Iterator localIterator1;

                        Iterator localIterator2;

                        if (MonsieurCannibale.this.speed > 0.0D) {

                            MCCup.setDistance(1.8D);

                            MonsieurCannibale.this.speed -= 0.01D;

                            MonsieurCannibale.this.setSpeed(MonsieurCannibale.this.speed);

                            for (localIterator1 = MonsieurCannibale.this.disks.iterator(); localIterator1.hasNext();


                                 localIterator2.hasNext()) {

                                MCDisk disk = (MCDisk) localIterator1.next();


                                disk.setSpeed(-MonsieurCannibale.this.speed);

                                localIterator2 = disk.getCups().iterator();
                                MCCup cup = (MCCup) localIterator2.next();

                                cup.setSpeed(MonsieurCannibale.this.speed);

                            }


                        } else {

                            MonsieurCannibale.this.setSpeed(0.0D);

                            MCCup.setDistance(1.0D);

                            for (localIterator1 = MonsieurCannibale.this.disks.iterator(); localIterator1.hasNext();

                                 localIterator2.hasNext()) {

                                MCDisk disk = (MCDisk) localIterator1.next();

                                disk.setSpeed(-MonsieurCannibale.this.speed);

                                localIterator2 = disk.getCups().iterator();
                                MCCup cup = (MCCup) localIterator2.next();

                                cup.setSpeed(MonsieurCannibale.this.speed);

                            }


                            MonsieurCannibale.this.started = false;


                            cancel();

                        }

                    }

                }

            }.runTaskTimer(Main.pl, 0L, 1L);

        }

    }


    public void setSpeed(double speed) {

        if (speed > 0.0D) {

            this.speed = speed;

        }

    }


    public void update() {

        for (int i = 0; i < this.disks.size(); i++) {

            MCDisk disk = this.disks.get(i);

            double add = 360 / this.disks.size() * i;

            Location to = LocationUtil.getRelative(this.center, 6.8D, this.angle + add);

            disk.moveTo(to);

        }

    }

}


